#!/bin/bash

# Error MSG
#	0	문제 없음.
#	17	디렉토리가 이미 존재함.

# root 계정으로 실행했는지 확인
RUN_ACCOUNT=$(whoami)

if [ "${RUN_ACCOUNT}" == "root" ]; then
	# 설치 계정 추출
	INSTALL_ACCOUNT=$(who -m | cut -f1 -d\ )
else
	# root 권한으로 실행 요청
	echo "Please, execute it as Root."
	exit 1
fi

# 파일 명을 배열에 저장
FILE_NAMES=("env_var" "create_user" "create_repo" "remove_user" "remove_repo")

# 서버 구동 계정 추출
if [ -n "$(dpkg -l | grep tomcat)" ]; then
	TOMCAT_ACCOUNT=$(ps -ef | grep tomcat | cut -f1 -d\  | uniq | sed '/root/d')

	if [ "${INSTALL_ACCOUNT}" != "${TOMCAT_ACCOUNT}" ]; then
		gpasswd -a ${TOMCAT_ACCOUNT} ${INSTALL_ACCOUNT}
	fi
else
	echo "There is no \"Tomcat\"."
	exit 1
	# TOMCAT_ACCOUNT=${INSTALL_ACCOUNT}
fi

# 설치 경로 설정
# 인자로 설치 경로가 주어지지 않으면 설치 스크립트를 실행하는 계정의 홈 디렉토리에 설치
if [ $# -eq 0 ]; then
	INSTALL_PATH=$(sudo -u ${INSTALL_ACCOUNT} echo $HOME)
else
	INSTALL_PATH="$1"
fi

INSTALL_PATH="${INSTALL_PATH}/cave-server"

# Git-shell 경로 추출
GIT_SHELL=$(which git-shell)

if [ -n "${GIT_SHELL}" ]; then
	SHELL_PATH=${GIT_SHELL}
else
	SHELL_PATH="/bin/sh"
fi

# 설치 디렉토리 존재 확인
if [ -e "${INSTALL_PATH}" ]; then
	# 디렉토리가 이미 존재하면 삭제할 지 물어봄
	echo "${INSTALL_PATH} is already exist."
	echo "Do you want to delete it and continue?(y or n)"
	read ANSWER

	while [ \( ! \( "${ANSWER}" == "y" \) \) -a \( ! \( "${ANSWER}" == "n" \) \) ]; do
		echo "Please input answer again.(y or n)"
		read ANSWER
	done

	if [ "${ANSWER}" == "y" ]; then
		# cave-server 디렉토리 삭제
		rm -rf ${INSTALL_PATH}

		# sudoers에 추가된 라인을 지우기 위한 방법
		sed -i '/env_var/d' /etc/sudoers
		sed -i '/create_user/d' /etc/sudoers
		sed -i '/create_repo/d' /etc/sudoers
		sed -i '/remove_user/d' /etc/sudoers
		sed -i '/remove_repo/d' /etc/sudoers
	else
		exit 17
	fi
fi

# 설치 디렉토리 및 하위 디렉토리 생성
mkdir ${INSTALL_PATH} -m 755
mkdir ${INSTALL_PATH}/repository -m 755
mkdir ${INSTALL_PATH}/sh -m 750

# 디렉토리 권한 변경
chown ${INSTALL_ACCOUNT}:${INSTALL_ACCOUNT} ${INSTALL_PATH} -R

# env_var 파일 생성
echo "#!/bin/bash

# 서버 구동 계정
SERVER_ACCOUNT=\"${TOMCAT_ACCOUNT}\"

# cave-server 설치 경로
CAVE_SERVER_PATH=\"${INSTALL_PATH}\"

# 계정들이 사용할 쉘 경로
SHELL_PATH=\"${SHELL_PATH}\"" > ./${FILE_NAMES[0]}

# 권한 변경
chmod 700 ./${FILE_NAMES[0]}

# 파일 복사, 소유자 변경 및 NOPASSWD 설정
for NAME in "${FILE_NAMES[@]}"; do
	cp ./${NAME} ${INSTALL_PATH}/sh
	chown root:root ${INSTALL_PATH}/sh/${NAME}
	echo "${TOMCAT_ACCOUNT} ALL=(ALL) NOPASSWD: ${INSTALL_PATH}/sh/${NAME}" >> /etc/sudoers
done
