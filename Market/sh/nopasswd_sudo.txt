sudo 실행 시 비밀번호 안물어보게 하기

1. 쉘 스크립트 작성
2. 스크립트 파일 수정 권한을 root에게만 허용(다른 사람한테 허용하면 수정해서 악용될 수 있음)
3. sudo visudo 로 sudo 권한 파일(sudoers) 오픈
4. %sudo	ALL=(ALL:ALL) ALL 라인을 찾는다.
5. 그 아래에 [사용자 이름] ALL=(ALL) NOPASSWD: [스크립트 절대 경로] 추가
6. 그 다음 sudo로 실행 시키면 비밀번호를 물어보지 않음.
