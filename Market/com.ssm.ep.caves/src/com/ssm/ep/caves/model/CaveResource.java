package com.ssm.ep.caves.model;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Date;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.PlatformUI;


// CaveResource 클래스는 IResource 객체를 포함하며 
// IResource 객체를 ICaveItem 인터페이스에 적응시킨다

public class CaveResource implements ICaveItem {
	private CaveItemType type;
	private IResource resource;
	private String name;
	private String rit = "Untracked";

	CaveResource(CaveItemType type, IResource resource) {
		this.type = type;
		this.resource = resource;
	}

	public static CaveResource loadCave(CaveItemType type,
			String info) {
		IResource res = ResourcesPlugin.getWorkspace().getRoot().findMember(
				new Path(info));
		if (res == null)
			return null;
		return new CaveResource(type, res);
	}

	public String getName() {
		if (name == null)
			name = resource.getName();
		return name;
	}

	public void setName(String newName) {
		name = newName;
	}

	public String getLocation() {
		IPath path = resource.getLocation().removeLastSegments(1);
		if (path.segmentCount() == 0)
			return "";
		return path.toString();
	}
	
	public String getFileSize() {
		File file = new File(resource.getLocationURI());
		Long fsize = file.length();
//		System.out.println("fs_others : " + fsize + " 바이트");
		if (fsize == 0)
			return "";
		return fsize + " bytes";
	}
	
	public String getMod() throws MalformedURLException {
		URI uri = resource.getLocationURI();
		Date dt = null;
		try {
			dt = new Date(uri.toURL().openConnection().getLastModified());
		} catch (IOException e) {
			e.printStackTrace();
		}
//		System.out.println("lm_others : " + dt.toString());
		if (uri == null){
			return "";
		}
		return dt.toString();
	}
	
	public boolean isCaveFor(Object obj) {
		return resource.equals(obj);
	}

	public CaveItemType getType() {
		return type;
	}

	public boolean equals(Object obj) {
		return this == obj
				|| ((obj instanceof CaveResource) && resource
						.equals(((CaveResource) obj).resource));
	}

	public int hashCode() {
		return resource.hashCode();
	}

	@SuppressWarnings("unchecked")
	public Object getAdapter(Class adapter) {
      return getAdapterDelegate(adapter);
   }

	private Object getAdapterDelegate(Class<?> adapter) {
		if (adapter.isInstance(resource))
			return resource;
		return Platform.getAdapterManager().getAdapter(this, adapter);
	}

	public String getInfo() {
		return resource.getFullPath().toString();
	}

	@Override
	public void setRit(String newRit) {
		rit = newRit;
	}

	@Override
	public String getRit() {
		return rit;
	}

}
