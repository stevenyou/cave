package com.ssm.ep.caves.model;

import java.util.EventObject;

public class CavesManagerEvent extends EventObject {
	private static final long serialVersionUID = 3697053173951102953L;

	private final ICaveItem[] added;
	private final ICaveItem[] removed;

	public CavesManagerEvent(CavesManager source,
			ICaveItem[] itemsAdded, ICaveItem[] itemsRemoved) {
		super(source);
		added = itemsAdded;
		removed = itemsRemoved;
	}

	public ICaveItem[] getItemsAdded() {
		return added;
	}

	public ICaveItem[] getItemsRemoved() {
		return removed;
	}
	
}
