package com.ssm.ep.caves.model;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Date;

import org.eclipse.core.resources.FileInfoMatcherDescription;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.util.FieldInfo;

import com.ssm.ep.caves.CavesLog;


public class CaveJavaElement implements ICaveItem {

	private CaveItemType type;
	private IJavaElement element;
	private String name;
	private String rit = "Untracked";

	public CaveJavaElement(CaveItemType type, IJavaElement element) {
		this.type = type;
		this.element = element;
	}

	public static CaveJavaElement loadCave(CaveItemType type,
			String info) {
		IResource res = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(info));
		if (res == null)
			return null;
		IJavaElement elem = JavaCore.create(res);
		if (elem == null)
			return null;
		return new CaveJavaElement(type, elem);
	}

	public String getName() {
		if (name == null)
			name = element.getElementName();
		return name;
	}

	public void setName(String newName) {
		name = newName;
	}
	
	public String getRit(){
		return name;
	}
	public void setRit(String newRit) {
		rit = newRit;
	}

	public String getLocation() {
		try {
			IResource res = element.getUnderlyingResource();
			if (res != null) {
				IPath path = res.getLocation().removeLastSegments(1);
				if (path.segmentCount() == 0)
					return "";
				return path.toString();
			}
		} catch (JavaModelException e) {
			CavesLog.logError(e);
		}
		return "";
	}

	public String getFileSize() {
		try {
			IResource res = element.getUnderlyingResource();
			File file = new File(res.getLocationURI());
			if (res != null) {
				Long fsize = file.length();
				if (fsize == 0)
					return "";
				return fsize + " bytes";
			}
		} catch (JavaModelException e) {
			CavesLog.logError(e);
		}
		return "";
	}
	
	public String getMod() throws MalformedURLException {
		try {
			IResource res = element.getUnderlyingResource();
			if (res != null) {
				URI uri = res.getLocationURI();
				Date dt = null;
				try {
					dt = new Date(uri.toURL().openConnection().getLastModified());
				} catch (IOException e) {
					e.printStackTrace();
				}
//				System.out.println("lm_java : " + dt.toString());
				if (uri == null){
					return "";
				}
				return dt.toString();
			}
		} catch (JavaModelException e) {
			CavesLog.logError(e);
		}
		return "";
	}
	

	public boolean isCaveFor(Object obj) {
		return element.equals(obj);
	}

	public CaveItemType getType() {
		return type;
	}

	public boolean equals(Object obj) {
		return this == obj
				|| ((obj instanceof CaveJavaElement) && element
						.equals(((CaveJavaElement) obj).element));
	}

	public int hashCode() {
		return element.hashCode();
	}

	@SuppressWarnings("unchecked")
	public Object getAdapter(Class adapter) {
		return getAdapterDelegate(adapter);
	}

	private Object getAdapterDelegate(Class<?> adapter) {
		if (adapter.isInstance(element))
			return element;
		IResource resource = element.getResource();
		if (adapter.isInstance(resource))
			return resource;
		return Platform.getAdapterManager().getAdapter(this, adapter);
	}

	public String getInfo() {
		try {
			return element.getUnderlyingResource().getFullPath().toString();
		} catch (JavaModelException e) {
			CavesLog.logError(e);
			return null;
		}
	}

}
