package com.ssm.ep.caves.model;

public interface CavesManagerListener {
	public void cavesChanged(CavesManagerEvent event);
}
