package com.ssm.ep.caves.model;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.jdt.ui.JavaUI;

import com.ssm.ep.caves.CavesLog;

public abstract class CaveItemType implements Comparable {
	private static final ISharedImages PLAYFORM_IMAGES = PlatformUI
			.getWorkbench().getSharedImages();
	private final String id;
	private final String printName;
	private final int ordinal;

	private CaveItemType(String id, String name, int positoin) {
		this.id = id;
		this.ordinal = positoin;
		this.printName = name;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return printName;
	}

	public abstract Image getImage();

	public abstract ICaveItem newCave(Object obj);

	public abstract ICaveItem loadCave(String info);

	@Override
	public int compareTo(Object arg) {
		return this.ordinal - ((CaveItemType) arg).ordinal;
	}

	public static final CaveItemType UNKNOWN = new CaveItemType("Unknown",
			"Unknown", 0) {
		public Image getImage() {
			return null;
		}

		public ICaveItem newCave(Object obj) {
			return null;
		}

		public ICaveItem loadCave(String info) {
			return null;
		}
	};

	public static final CaveItemType WORKBENCH_FILE = new CaveItemType(
			"WBFile", "Workbench File", 1) {
		public Image getImage() {
			return PlatformUI.getWorkbench().getSharedImages()
					.getImage(org.eclipse.ui.ISharedImages.IMG_OBJ_FILE);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IFile))
				return null;
			return new CaveResource(this, (IFile) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveResource.loadCave(this, info);
		}
	};

	public static final CaveItemType WORKBENCH_FOLDER = new CaveItemType(
			"WBFolder", "Workbench Folder", 2) {
		public Image getImage() {
			return PlatformUI.getWorkbench().getSharedImages()
					.getImage(org.eclipse.ui.ISharedImages.IMG_OBJ_FOLDER);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IFolder))
				return null;
			return new CaveResource(this, (IFolder) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveResource.loadCave(this, info);
		}
	};

	public static final CaveItemType WORKBENCH_PROJECT = new CaveItemType(
			"WBProj", "WorkbenchProject", 3) {
		public Image getImage() {
			return PlatformUI.getWorkbench().getSharedImages()
					.getImage(IDE.SharedImages.IMG_OBJ_PROJECT);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IProject))
				return null;
			return new CaveResource(this, (IProject) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveResource.loadCave(this, info);
		}
	};

	public static final CaveItemType JAVA_PROJECT = new CaveItemType("JProj",
			"Java Project", 4) {
		public Image getImage() {
			return PlatformUI.getWorkbench().getSharedImages()
					.getImage(IDE.SharedImages.IMG_OBJ_PROJECT);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IJavaProject))
				return null;
			return new CaveJavaElement(this, (IJavaProject) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveJavaElement.loadCave(this, info);
		}
	};

	public static final CaveItemType JAVA_PACKAGE_ROOT = new CaveItemType(
			"JPkgRoot", "Java Package Root", 5) {
		public Image getImage() {
			return PlatformUI.getWorkbench().getSharedImages()
					.getImage(org.eclipse.ui.ISharedImages.IMG_OBJ_FOLDER);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IPackageFragmentRoot))
				return null;
			return new CaveJavaElement(this, (IPackageFragmentRoot) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveJavaElement.loadCave(this, info);
		}
	};

	public static final CaveItemType JAVA_PACKAGE = new CaveItemType("JPkg",
			"Java Package", 6) {
		public Image getImage() {
			return org.eclipse.jdt.ui.JavaUI.getSharedImages().getImage(
					org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_PACKAGE);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IPackageFragment))
				return null;
			return new CaveJavaElement(this, (IPackageFragment) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveJavaElement.loadCave(this, info);
		}
	};

	public static final CaveItemType JAVA_CLASS_FILE = new CaveItemType(
			"JClass", "Java Class File", 7) {
		public Image getImage() {
			return org.eclipse.jdt.ui.JavaUI.getSharedImages().getImage(
					org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_CFILE);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IClassFile))
				return null;
			return new CaveJavaElement(this, (IClassFile) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveJavaElement.loadCave(this, info);
		}
	};

	public static final CaveItemType JAVA_COMP_UNIT = new CaveItemType(
			"JCompUnit", "Java Compilation Unit", 8) {
		public Image getImage() {
			return org.eclipse.jdt.ui.JavaUI.getSharedImages().getImage(
					org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_CUNIT);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof ICompilationUnit))
				return null;
			return new CaveJavaElement(this, (ICompilationUnit) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveJavaElement.loadCave(this, info);
		}
	};

	public static final CaveItemType JAVA_INTERFACE = new CaveItemType(
			"JInterface", "Java Interface", 9) {
		public Image getImage() {
			return org.eclipse.jdt.ui.JavaUI.getSharedImages().getImage(
					org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_INTERFACE);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IType))
				return null;
			try {
				if (!((IType) obj).isInterface())
					return null;
			} catch (JavaModelException e) {
				CavesLog.logError(e);
			}
			return new CaveJavaElement(this, (IType) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveJavaElement.loadCave(this, info);
		}
	};

	public static final CaveItemType JAVA_CLASS = new CaveItemType("JClass",
			"Java Class", 10) {
		public Image getImage() {
			return org.eclipse.jdt.ui.JavaUI.getSharedImages().getImage(
					org.eclipse.jdt.ui.ISharedImages.IMG_OBJS_CLASS);
		}

		public ICaveItem newCave(Object obj) {
			if (!(obj instanceof IType))
				return null;
			try {
				if (((IType) obj).isInterface())
					return null;
			} catch (JavaModelException e) {
				CavesLog.logError(e);
			}
			return new CaveJavaElement(this, (IType) obj);
		}

		public ICaveItem loadCave(String info) {
			return CaveJavaElement.loadCave(this, info);
		}
	};

	private static final CaveItemType[] TYPES = { UNKNOWN, WORKBENCH_FILE,
			WORKBENCH_FOLDER, WORKBENCH_PROJECT, JAVA_PROJECT,
			JAVA_PACKAGE_ROOT, JAVA_PACKAGE, JAVA_CLASS_FILE, JAVA_COMP_UNIT,
			JAVA_INTERFACE, JAVA_CLASS };

	public static CaveItemType[] getTypes() {
		return TYPES;
	}

}
