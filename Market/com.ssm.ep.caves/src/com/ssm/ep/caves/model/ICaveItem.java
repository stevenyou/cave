package com.ssm.ep.caves.model;

import java.net.MalformedURLException;

import org.eclipse.core.runtime.IAdaptable;


// ICaveItem 인터페이스 ---> 다양한 객체 유형을 추상화
public interface ICaveItem extends IAdaptable{
	// get,set - Name
	String getName();
	void setName(String name);
	// get - Location
	String getLocation();
	// bool - what kinds of object
	boolean isCaveFor(Object obj);
	// get - Type
	CaveItemType getType();
	// get - Info
	String getInfo();
	String getMod() throws MalformedURLException;
	String getFileSize();
	void setRit(String rit);
	String getRit();
	
	static ICaveItem[] NONE = new ICaveItem[] {};
}
