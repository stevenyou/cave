package com.ssm.ep.caves.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.CreateBranchCommand.SetupUpstreamMode;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRefNameException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.RefAlreadyExistsException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.XMLMemento;

import com.ssm.ep.caves.CavesActivator;
import com.ssm.ep.caves.CavesLog;
import com.ssm.ep.caves.gitcommand.commitGit;
import com.ssm.ep.caves.gitcommand.removeGit;
import com.ssm.ep.caves.gitcommand.stageGit;
import com.ssm.ep.caves.util.CheckInit;
import com.ssm.ep.caves.util.KillProcess;
import com.ssm.ep.caves.util.RunTimeGit;
import com.ssm.ep.caves.util.processCatcher_;

public class CavesManager {

	private static final String TAG_CAVES = "Caves";
	private static final String TAG_CAVE = "Cave";
	private static final String TAG_TYPEID = "TypeId";
	private static final String TAG_INFO = "Info";

	private static CavesManager manager;
	private Collection<ICaveItem> caves;
	private List<CavesManagerListener> listeners = new ArrayList<CavesManagerListener>();

	private CavesManager() {
	}

	public static CavesManager getManager() {
		if (manager == null)
			manager = new CavesManager();
		return manager;
	}

	public ICaveItem[] getCaves() {
		if (caves == null)
			loadCaves();
		return caves.toArray(new ICaveItem[caves.size()]);
	}

	public void addCaves(Object[] objects) {
		if (objects == null)
			return;
		if (caves == null)
			loadCaves();
		Collection<ICaveItem> items = new HashSet<ICaveItem>(objects.length);
		for (int i = 0; i < objects.length; i++) {
			ICaveItem item = existingCaveFor(objects[i]);
			if (item == null) {
				item = newCaveFor(objects[i]);
				if (caves.add(item))
					items.add(item);
			}
		}
		if (items.size() > 0) {
			ICaveItem[] added = items.toArray(new ICaveItem[items.size()]);
			fireCavesChanged(added, ICaveItem.NONE);
		}
	}

	public void outCaves(Object[] objects) {
		if (objects == null)
			return;
		if (caves == null)
			loadCaves();
		Collection<ICaveItem> items = new HashSet<ICaveItem>(objects.length);
		for (int i = 0; i < objects.length; i++) {
			ICaveItem item = existingCaveFor(objects[i]);
			if (item != null && caves.remove(item))
				items.add(item);
		}
		if (items.size() > 0) {
			ICaveItem[] removed = items.toArray(new ICaveItem[items.size()]);
			fireCavesChanged(ICaveItem.NONE, removed);
		}
	}

	public void removeCaves(Object[] objects) {
		if (objects == null)
			return;
		if (caves == null)
			loadCaves();
		Collection<ICaveItem> items = new HashSet<ICaveItem>(objects.length);
		for (int i = 0; i < objects.length; i++) {
			ICaveItem item = existingCaveFor(objects[i]);
			if (item != null && caves.remove(item))
				items.add(item);
		}
		if (items.size() > 0) {
			ICaveItem[] removed = items.toArray(new ICaveItem[items.size()]);

			// rm in the Client GIT
			for (ICaveItem i : items) {
				String name = i.getName();
				String dir = i.getLocation();
				String initDir = dir.substring(0, dir.lastIndexOf("src") - 1);
				System.out.println("*  rm -----  Name : " + name);
				System.out.println("*  rm -----  Location : " + dir);
				System.out.println("*  rm -----  initDir : " + initDir);

				RunTimeGit rg = new RunTimeGit(CavesActivator.dummy1,
						CavesActivator.dir);
				CheckInit ck = new CheckInit();
				try {
					rg.process = rg.runTime.exec(rg.cmd);
					removeGit so = new removeGit(rg.process.getOutputStream(),
							name, dir, initDir, ck.isContainList(initDir));
					so.start();
					while (true) {
						if (!so.isAlive()) {
							rg.process.waitFor();
							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (rg.process != null)
						rg.process.destroy();
				}
			}
			fireCavesChanged(ICaveItem.NONE, removed);
		}
	}

	public int stageCaves(Object[] objects) {
		int changed = 0;
		if (objects == null)
			return 0;
		if (caves == null)
			loadCaves();
		Collection<ICaveItem> items = new HashSet<ICaveItem>(objects.length);
		for (int i = 0; i < objects.length; i++) {
			ICaveItem item = existingCaveFor(objects[i]);
			if (item != null && caves.remove(item)) {
				item.setRit("Staged");
				changed++;
			}
			items.add(item);
		}
		if (items.size() > 0) {

			for (ICaveItem i : items) {
				String name = i.getName();
				String dir = i.getLocation();
				String initDir = "";
				String remanant = "";
				if(dir.contains("src")){
					initDir = dir.substring(0, dir.lastIndexOf("src") - 1);
					remanant = dir.substring(dir.lastIndexOf("src") - 1);
					remanant = remanant.replace("src", "bin");
				} else {
					initDir = dir;
				}
				String classFilePath = initDir + remanant;
				System.out.println("*  staged -----  Name : " + name);
				System.out.println("*  staged -----  Location : " + dir);
				System.out.println("*  staged -----  initDir : " + initDir);
				System.out.println("*  staged -----  remanant : " + remanant);
				System.out.println("*  staged -----  classFilePath : " + classFilePath);

				RunTimeGit rg = new RunTimeGit(CavesActivator.dummy1,
						CavesActivator.dir);
				CheckInit ck = new CheckInit();
				try {
					rg.process = rg.runTime.exec(rg.cmd);
					stageGit so = new stageGit(rg.process.getOutputStream(),
							name, dir, initDir, ck.isContainList(initDir), classFilePath);
					so.start();
					while (true) {
						if (!so.isAlive()) {
							rg.process.waitFor();
							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (rg.process != null)
						rg.process.destroy();
				}

				caves.add(i);
			}
		}
		return changed;
	}

	public int pushCaves(Object[] objects) {
		int changed = 0;
		if (objects == null)
			return 0;
		if (caves == null)
			loadCaves();
		Collection<ICaveItem> items = new HashSet<ICaveItem>(objects.length);
		for (int i = 0; i < objects.length; i++) {
			ICaveItem item = existingCaveFor(objects[i]);
			if (item != null && caves.remove(item)) {
				if (item.getRit().equals("Commited")) {
					item.setRit("Pushed");
					changed++;
				}
				items.add(item);
			}
		}
		if (items.size() > 0) {

			for (ICaveItem i : items) {
				String name = i.getName();
				String dir = i.getLocation();
				String initDir;
				if(dir.contains("src")){
					initDir = dir.substring(0, dir.lastIndexOf("src") - 1);
				} else {
					initDir = dir;
				}
				System.out.println("*  pushed -----  Name : " + name);
				System.out.println("*  pushed -----  Location : " + dir);
				System.out.println("*  pushed -----  initDir : " + initDir);
				
				
				String push_url = null;
				String PropFile = "C:\\lecture\\runtime-EclipseApplication\\.metadata\\.plugins\\org.eclipse.core.runtime"
						+ "\\.settings\\com.ssm.ep.caves.prefs";
				String propLine = null;

				try (BufferedReader reader = new BufferedReader(new InputStreamReader(
						new FileInputStream(PropFile),
						System.getProperty("file.encoding")));) {
					while ((propLine = reader.readLine()) != null) {
						if (propLine.split("=")[0].equals("push")) {
							push_url = propLine.split("=")[1];
						}
					}
				} catch(IOException e){
					e.printStackTrace();
				}
				
				String cmd[] = new String[3];
				String entre = System.getProperty("line.separator");
				cmd[0] = "cmd.exe";
				cmd[1] = "/C";
				cmd[2] = "cd " + CavesActivator.dummy1 + CavesActivator.dir
						+ "&&sh.exe --login -i";
				CheckInit ck = new CheckInit();
				Runtime runTime = Runtime.getRuntime();
				Process process = null;
				
				
				if(!ck.isAlreadyCheckSSH(push_url)){
					System.out.println("execute ssh protocol key gen");
					 try {
						 process = runTime.exec(cmd);
						 PrintWriter out = new PrintWriter(process.getOutputStream());
						 processCatcher_ gbe = new processCatcher_(process.getErrorStream(), out, push_url);
						 gbe.start();
							while (true) {
								if (!gbe.isAlive()) {
									System.out.println("thread_ssh_END_PRE");
	//								process.waitFor();
									break;
								}
							}
							System.out.println("thread_ssh_END");
					 } catch (Exception e) {
						 e.printStackTrace();
					 } finally {
						 if (process != null)
							 System.out.println("PROCESS_ssh_END");
							 process.destroy();
					}
				}
				 
				// ssh.exe task kill
				KillProcess.killSSH();
				
				String user = CavesActivator.user;
				String password = CavesActivator.password;
				String localPath = initDir;
				String remotePath = CavesActivator.push;
				Git localGit = null;
				
				try {
					localGit = Git.open(new File(localPath));
				} catch (IOException e2) {
					e2.printStackTrace();
				}
				try {
					localGit.checkout().setName("refs/heads/master")
							.setUpstreamMode(SetupUpstreamMode.TRACK).call();
				} catch (RefAlreadyExistsException e1) {
					e1.printStackTrace();
				} catch (RefNotFoundException e1) {
					e1.printStackTrace();
				} catch (InvalidRefNameException e1) {
					e1.printStackTrace();
				} catch (CheckoutConflictException e1) {
					e1.printStackTrace();
				} catch (GitAPIException e1) {
					e1.printStackTrace();
				}
				PushCommand push = localGit.push();
				UsernamePasswordCredentialsProvider user_ = new UsernamePasswordCredentialsProvider(
						user, password);
				push.setCredentialsProvider(user_);
				push.setRemote(remotePath);
				try {
					java.util.Iterator<PushResult> itr = push.call().iterator();
				} catch (InvalidRemoteException e) {
					e.printStackTrace();
				} catch (TransportException e) {
					e.printStackTrace();
				} catch (GitAPIException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				} 
				
				caves.add(i);
			}
		}
		return changed;
	}

	public int commitCaves(Object[] objects) {
		int changed = 0;
		if (objects == null)
			return 0;
		if (caves == null)
			loadCaves();
		Collection<ICaveItem> items = new HashSet<ICaveItem>(objects.length);
		for (int i = 0; i < objects.length; i++) {
			ICaveItem item = existingCaveFor(objects[i]);
			if (item != null && caves.remove(item)) {
				if (item.getRit().equals("Staged")) {
					item.setRit("Commited");
					changed++;
				}
				items.add(item);
			}
		}

		System.out.println("# COMMIT #");

		if (items.size() > 0) {

			for (ICaveItem i : items) {
				String dir = i.getLocation();
				if(dir.contains("src")){
					dir = dir.substring(0, dir.lastIndexOf("src") - 1);
				}
				System.out.println("*  commit -----  Location : " + dir);

				RunTimeGit rg = new RunTimeGit(CavesActivator.dummy1,
						CavesActivator.dir);
				CheckInit ck = new CheckInit();
				try {
					rg.process = rg.runTime.exec(rg.cmd);
					commitGit so = new commitGit(rg.process.getOutputStream(),
							dir, ck.isContainList(dir));
					so.start();
					while (true) {
						if (!so.isAlive()) {
							rg.process.waitFor();
							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (rg.process != null)
						rg.process.destroy();
				}

				caves.add(i);
			}
		}

		return changed;
	}

	public ICaveItem newCaveFor(Object obj) {
		CaveItemType[] types = CaveItemType.getTypes();
		for (int i = 0; i < types.length; i++) {
			ICaveItem item = types[i].newCave(obj);
			if (item != null)
				return item;
		}
		return null;
	}

	private ICaveItem existingCaveFor(Object obj) {
		if (obj == null)
			return null;
		if (obj instanceof ICaveItem)
			return (ICaveItem) obj;
		Iterator<ICaveItem> iter = caves.iterator();
		while (iter.hasNext()) {
			ICaveItem item = iter.next();
			if (item.isCaveFor(obj))
				return item;
		}
		return null;
	}

	public ICaveItem[] existingCavesFor(Iterator<?> iter) {
		List<ICaveItem> result = new ArrayList<ICaveItem>(10);
		while (iter.hasNext()) {
			ICaveItem item = existingCaveFor(iter.next());
			if (item != null)
				result.add(item);
		}
		return (ICaveItem[]) result.toArray(new ICaveItem[result.size()]);
	}

	// Event Handling

	public void addCavesManagerListener(CavesManagerListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeCavesManagerListener(CavesManagerListener listener) {
		listeners.remove(listener);
	}

	private void fireCavesChanged(ICaveItem[] itemsAdded,
			ICaveItem[] itemsRemoved) {
		CavesManagerEvent event = new CavesManagerEvent(this, itemsAdded,
				itemsRemoved);
		for (Iterator<CavesManagerListener> iter = listeners.iterator(); iter
				.hasNext();)
			iter.next().cavesChanged(event);
	}

	// Persisting caves

	private void loadCaves() {
		caves = new HashSet<ICaveItem>(20);
		FileReader reader = null;
		try {
			reader = new FileReader(getCavesFile());
			loadCaves(XMLMemento.createReadRoot(reader));
		} catch (FileNotFoundException e) {
			// Ignored... no Caves items exist yet.
		} catch (Exception e) {
			// Log the exception and move on.
			CavesLog.logError(e);
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				CavesLog.logError(e);
			}
		}
	}

	private void loadCaves(XMLMemento memento) {
		IMemento[] children = memento.getChildren(TAG_CAVE);
		for (int i = 0; i < children.length; i++) {
			ICaveItem item = newCaveFor(children[i].getString(TAG_TYPEID),
					children[i].getString(TAG_INFO));
			if (item != null)
				caves.add(item);
		}
	}

	public ICaveItem newCaveFor(String typeId, String info) {
		CaveItemType[] types = CaveItemType.getTypes();
		for (int i = 0; i < types.length; i++)
			if (types[i].getId().equals(typeId))
				return types[i].loadCave(info);
		return null;
	}

	public void saveCaves() {
		if (caves == null)
			return;
		XMLMemento memento = XMLMemento.createWriteRoot(TAG_CAVES);
		saveCaves(memento);
		FileWriter writer = null;
		try {
			writer = new FileWriter(getCavesFile());
			memento.save(writer);
		} catch (IOException e) {
			CavesLog.logError(e);
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				CavesLog.logError(e);
			}
		}
	}

	private void saveCaves(XMLMemento memento) {
		Iterator<ICaveItem> iter = caves.iterator();
		while (iter.hasNext()) {
			ICaveItem item = iter.next();
			IMemento child = memento.createChild(TAG_CAVE);
			child.putString(TAG_TYPEID, item.getType().getId());
			child.putString(TAG_INFO, item.getInfo());
		}
	}

	private File getCavesFile() {
		return CavesActivator.getDefault().getStateLocation()
				.append("caves.xml").toFile();
	}

}
