package com.ssm.ep.caves.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
public class CutCavesHandler extends AbstractHandler
{
   IHandler copy = new CopyCavesHandler();
   IHandler remove = new RemoveCavesHandler();

   public Object execute(ExecutionEvent event)
         throws ExecutionException {
      copy.execute(event);
      remove.execute(event);
      return null;
   }

   public void dispose() {
      copy.dispose();
      remove.dispose();
      super.dispose();
   }
}
