package com.ssm.ep.caves.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;

import com.ssm.ep.caves.views.CavesView;


public class RenameCavesHandler extends AbstractHandler
{
   private static final int COLUMN_TO_EDIT = 1;

   public Object execute(ExecutionEvent event)
         throws ExecutionException {
      IWorkbenchPart part = HandlerUtil.getActivePart(event);
      if (!(part instanceof CavesView))
         return null;
      editElement((CavesView) part);
      return null;
   }

   public void editElement(CavesView CavesView) {
      TableViewer viewer =
            CavesView.getCavesViewer();
      IStructuredSelection selection =
            (IStructuredSelection) viewer.getSelection();
      if (!selection.isEmpty())
         viewer.editElement(selection.getFirstElement(), COLUMN_TO_EDIT);
   }
}
