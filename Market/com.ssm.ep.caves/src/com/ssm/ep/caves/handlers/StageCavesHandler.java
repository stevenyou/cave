package com.ssm.ep.caves.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;

import com.ssm.ep.caves.model.CavesManager;
import com.ssm.ep.caves.views.CaveStagedDialog;
import com.ssm.ep.caves.views.CavesView;

public class StageCavesHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		int changed = 0;
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			changed = CavesManager.getManager().stageCaves(
					((IStructuredSelection) selection).toArray());
		}
		IWorkbenchPart part = HandlerUtil.getActivePart(event);
		if (!(part instanceof CavesView))
			return null;
		geneDialog((CavesView) part, changed);
		return null;
	}

	public void geneDialog(CavesView CavesView, int changed) {
		TableViewer viewer = CavesView.getCavesViewer();
		Shell shell = viewer.getControl().getShell();
		viewer.refresh();
		new CaveStagedDialog(shell, changed).open();
	}
}
