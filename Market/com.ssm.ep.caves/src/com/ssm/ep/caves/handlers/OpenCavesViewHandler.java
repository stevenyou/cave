package com.ssm.ep.caves.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import com.ssm.ep.caves.CavesLog;
import com.ssm.ep.caves.views.CavesView;


public class OpenCavesViewHandler extends AbstractHandler {

	/**
	 * Open the Caves view.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// Get the active window

		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		if (window == null)
			return null;

		// Get the active page

		IWorkbenchPage page = window.getActivePage();
		if (page == null)
			return null;

		// Open and activate the Caves view

		try {
			page.showView(CavesView.ID);
		} catch (PartInitException e) {
			CavesLog.logError("Failed to open the Caves view", e);
		}
		return null;
	}
}
