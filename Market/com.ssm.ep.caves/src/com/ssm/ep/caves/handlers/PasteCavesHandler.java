package com.ssm.ep.caves.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.part.ResourceTransfer;

import com.ssm.ep.caves.model.CavesManager;


public class PasteCavesHandler extends ClipboardHandler
{
   protected Object execute(ExecutionEvent event, Clipboard clipboard)
         throws ExecutionException {
      paste(clipboard, ResourceTransfer.getInstance());
      paste(clipboard, JavaUI.getJavaElementClipboardTransfer());
      return null;
   }

   private void paste(Clipboard clipboard, Transfer transfer) {
      Object[] elements = (Object[]) clipboard.getContents(transfer);
      if (elements != null && elements.length != 0)
         CavesManager.getManager().addCaves(elements);
   }
}
