package com.ssm.ep.caves.gitcommand;

import java.io.OutputStream;
import java.io.PrintWriter;

public class commitGit extends Thread {
	PrintWriter out;
	private String dir;
	private boolean ck;
	
	public commitGit (OutputStream out, String dir, boolean ck){
		this.out = new PrintWriter(out);
		this.dir = dir.replace("/", "\\\\");		
		this.ck = ck;
	}
	
	public void run() {
		out.println("cd " + dir);
		out.flush();
		if(!ck){
			out.println("git init");
			out.flush();
		}
		out.println("git commit -m 'Commit with Cave Eclipse Plugin : '");
		out.flush();
		out.close();
	}
}
