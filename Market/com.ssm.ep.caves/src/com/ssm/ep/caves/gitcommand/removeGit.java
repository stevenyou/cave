package com.ssm.ep.caves.gitcommand;

import java.io.OutputStream;
import java.io.PrintWriter;

public class removeGit extends Thread {
	PrintWriter out;
	private String name;
	private String dir;
	private String gdir;
	private boolean ck;
	
	public removeGit (OutputStream out, 
						  String name, String dir, String gdir, boolean ck){
		this.out = new PrintWriter(out);
		this.name = name;
		this.dir = dir.replace("/", "\\\\");
		this.gdir = gdir.replace("/", "\\\\");
		this.ck = ck;
	}
	
	public void run() {
		if (!ck) {
			out.println("cd " + gdir);
			out.flush();
			out.println("git init");
			out.flush();
		} else {
			System.out.println("* in : Git already init");
		}
		out.println("cd " + dir);
		out.flush();
		out.println("git rm --cached " + name);
		out.flush();
		out.close();
	}
}
