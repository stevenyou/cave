package com.ssm.ep.caves.gitcommand;

import java.io.OutputStream;
import java.io.PrintWriter;

import com.ssm.ep.caves.CavesActivator;


public class stageGit extends Thread {
	PrintWriter out;
	private String name;
	private String dir;
	private String gdir;
	private boolean ck;
	private String classFilePath;
	private String classFileName;
	
	public stageGit(OutputStream out, 
				String name, String dir, String gdir, boolean ck, String classFilePath) {
		this.out = new PrintWriter(out);
		this.name = name;
		this.dir = dir.replace("/", "\\\\");
		this.gdir = gdir.replace("/", "\\\\");
		this.ck = ck;
		this.classFilePath = classFilePath;
		this.classFileName = name.replace("java", "class");
	}
	
	public void run() {
		if (!ck) {
			out.println("cd " + gdir);
			out.flush();
			out.println("git init");
			out.flush();
		} else {
			System.out.println("* in : Git already init");
		}
		out.println("cd " + dir);
		out.flush();
		System.out.println("* in : git add "+ name);
		out.println("git add " + name);
		out.flush();
		
		if(name.endsWith(".java")){
			out.println("cd " + classFilePath);
			out.flush();
			out.println("git add -f " + classFileName );
			out.flush();
			System.out.println("* in : git add -f "+ classFileName);
		}
		out.close();
	}

}
