package com.ssm.ep.caves.gitcommand;

import java.io.OutputStream;
import java.io.PrintWriter;

public class InitGIt extends Thread {
	PrintWriter out;
	private String name;
	private String email;
	private String url;
	private String gdir;
	private boolean ck;
	
	public InitGIt (OutputStream out, String name, String email, 
					String url, String gdir, boolean ck){
		this.out = new PrintWriter(out);
		this.name = name;
		this.email = email;
		this.url = url;
		this.gdir = gdir;
		this.ck = ck;
	}
	
	public void run() {
		if(url.equals("*") || url == null){
			out.println("git config --global user.name " + name);
			out.flush();
			out.println("git config --global user.email " + email);
			out.flush();
		} else {
			out.println("git clone " + url );
			out.flush();
		}
		if(!gdir.equals("*")){
			out.println("cd" + gdir);
			//test
			System.out.println(" in * gdir : " + gdir);
			System.out.println(" in * ck : " + ck);
			if(!ck){
				out.println("git init");
			} else {
				System.out.println(" in *  : already init this project's path");
			}
			out.flush();
		}
		out.close();
	}

}
