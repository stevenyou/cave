package com.ssm.ep.caves.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.sound.sampled.Line;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;

import com.ssm.ep.caves.editors.CavePropertiesFileContents;

public class FileGenerater {


	public static void makeFile() {
		
		String PropFile = "C:\\lecture\\runtime-EclipseApplication\\.metadata\\.plugins\\org.eclipse.core.runtime"
				+ "\\.settings\\com.ssm.ep.caves.prefs";
		String propLine = null;
		String tproj= null;
		int targetIndex = 0;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(PropFile),
				System.getProperty("file.encoding")));) {
			while ((propLine = reader.readLine()) != null) {
				if (propLine.split("=")[0].equals("tproj")) {
					tproj = propLine.split("=")[1];
				}
			}
			System.out.println(" ** tragetPROJ : " + tproj);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		IResource resouces[] = null;
		try {
			resouces = ResourcesPlugin.getWorkspace().getRoot().members();
		} catch (CoreException e1) {
			e1.printStackTrace();
		}
		
		for(int i = 0 ; i < resouces.length; i++){
			if(tproj.equals(resouces[i].toString().substring(resouces[i].toString().lastIndexOf("/")+1))){
				targetIndex = i;
				break;
			}
		}
		
		String workspaceDir = resouces[targetIndex].getLocation().toString();
		System.out.println(" ****** workspaceDir : " + workspaceDir);
		
		String projectName = workspaceDir.substring(workspaceDir.lastIndexOf("/")+1);
		
		IProject project = ResourcesPlugin.getWorkspace().getRoot()
				.getProject(projectName);
		System.out.println(" ****** projectName : " + projectName);
		
		

		IPath targetPath = new Path(workspaceDir + project.getName());
		final IProjectDescription dscp = ResourcesPlugin.getWorkspace()
				.newProjectDescription(project.getName());
		dscp.setLocation(targetPath);
		if (!project.exists()) {
			try {
				project.create(dscp, null);
				project.open(null);
			} catch (CoreException e2) {
				e2.printStackTrace();
			}
		}
		IFile file = null;
		String filename = null;
		String packagename = null;
		System.out.println("sss : " + CavePropertiesFileContents.contents);
		for(String s : CavePropertiesFileContents.contents){
			if(s.contains("filename")){
				filename = s.split("=")[1];
			}
			if(s.contains("packagename=")){
				packagename = "";
			}
		}
		try {
			packagename= packagename.replace(".", "\\");
			if(packagename.equals("")) { //C project
				file = project.getFile(new Path("README_" + filename + ".txt"));
			} else {
				file = project.getFile(new Path("src\\" + packagename + "\\README_" + filename + ".txt"));
			}
		} catch (Exception e) {
			System.out.println("Save again");
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			FileConverter.convert(new OutputStreamWriter(baos));
		} catch (IOException e) {
			e.printStackTrace();
		}
		ByteArrayInputStream contents = new ByteArrayInputStream(
				baos.toByteArray());
		if (file.exists()) {
			try {
				file.setContents(contents, true, false, null);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		} else {
			try {
				file.create(contents, true, null);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}

	}
}
