package com.ssm.ep.caves.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StreamGobbler extends Thread {
	InputStream is;
//	int end;
//	int to= 0;

	public StreamGobbler(InputStream is) {
		this.is = is;
//		this.end = end;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			while ((line = br.readLine()) != null){
				System.out.println(" * " + this.getId() + " : " + line);
//				if(line.equals("$ ")){
//					to++;
//				}
//				System.out.println("   *** to : " + to);
//				if(to == end){
//					break;
//				}
			}
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
	}
}
