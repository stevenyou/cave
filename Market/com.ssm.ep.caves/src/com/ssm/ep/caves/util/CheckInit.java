package com.ssm.ep.caves.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class CheckInit {
	
	CheckGlistSer glist = null;     
	
	
	private static volatile CheckInit instance = null;
	
	public static CheckInit getInstance(){
		if(instance == null){
			synchronized (CheckInit.class){
				if(instance == null){
					instance = new CheckInit();
				}
			}
		}
		return instance;
	}
	
	public static void main(String[] args){
		try (ObjectOutputStream out = new ObjectOutputStream(
				new BufferedOutputStream(
						new FileOutputStream(
								"C:\\lecture\\runtime-EclipseApplication\\.metadata\\CheckGlist.ser")));) {
			out.writeObject(new CheckGlistSer());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean isContainList(String gdir) {
		
		try (ObjectInputStream in = new ObjectInputStream(
				new BufferedInputStream(new FileInputStream(
						"C:\\lecture\\runtime-EclipseApplication\\.metadata\\CheckGlist.ser")));) {
			glist = (CheckGlistSer) in.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(glist.gdirList.contains(gdir)){
			return true;
		} else {
			glist.gdirList.add(gdir);
			try (ObjectOutputStream out = new ObjectOutputStream(
					new BufferedOutputStream(new FileOutputStream(
							"C:\\lecture\\runtime-EclipseApplication\\.metadata\\CheckGlist.ser")));) {
				out.writeObject(glist);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
		
	}
	
	public boolean isAlreadyCheckSSH(String URL) {
		
		try (ObjectInputStream in = new ObjectInputStream(
				new BufferedInputStream(new FileInputStream(
						"C:\\lecture\\runtime-EclipseApplication\\.metadata\\CheckGlist.ser")));) {
			glist = (CheckGlistSer) in.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(glist.ssh_Key_submit_List.containsKey(URL)){
			if(glist.ssh_Key_submit_List.get(URL).equals("t")){
				return true;
			} else {
				glist.ssh_Key_submit_List.put(URL, "t");
				try (ObjectOutputStream out = new ObjectOutputStream(
						new BufferedOutputStream(new FileOutputStream(
								"C:\\lecture\\runtime-EclipseApplication\\.metadata\\CheckGlist.ser")));) {
					out.writeObject(glist);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}
		} else {
			glist.ssh_Key_submit_List.put(URL, "t");
			try (ObjectOutputStream out = new ObjectOutputStream(
					new BufferedOutputStream(new FileOutputStream(
							"C:\\lecture\\runtime-EclipseApplication\\.metadata\\CheckGlist.ser")));) {
				out.writeObject(glist);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
		
	}

}
