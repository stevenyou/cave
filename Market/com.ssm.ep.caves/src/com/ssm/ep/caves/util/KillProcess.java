package com.ssm.ep.caves.util;

import java.io.PrintWriter;

public class KillProcess {
	
	public static void killSSH(){
	
		Process process = null;
		String cmd_ssh[] = new String[3];
		cmd_ssh[0] = "cmd.exe";
		cmd_ssh[1] = "/C";
		cmd_ssh[2] = "taskkill /f /im ssh.exe /t";
		
		try {
			Runtime runTime_ = Runtime.getRuntime();
			Process process_ = runTime_.exec(cmd_ssh);
			PrintWriter out = new PrintWriter(process_.getOutputStream());
			processCatcher_kill gbe_ = new processCatcher_kill(process_.getInputStream(), out);
			gbe_.start();
			while (true) {
				if (!gbe_.isAlive()) {
					break;
				}
			}
			System.out.println("taskkill_end");
		} catch (Exception e) {
	
		} finally {
			if (process != null)
				process.destroy();
		}
	
	}
	
}
