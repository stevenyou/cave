package com.ssm.ep.caves.util;

import java.io.IOException;
import java.io.Writer;

import com.ssm.ep.caves.editors.CavePropertiesFileContents;

public class FileConverter {
	
	public static final String LINE_SEPERATOR=System.getProperty("line.separator");

	
	public static void convert(Writer writer) throws IOException{
		
		while(CavePropertiesFileContents.contents.size() != 0){
			writer.write(CavePropertiesFileContents.contents.get(0));
			writer.write(LINE_SEPERATOR);
			CavePropertiesFileContents.contents.remove(0);
		}
		writer.flush();
		writer.close();
		CavePropertiesFileContents.contents.clear();
		System.out.println("* Is Remain ? : " + CavePropertiesFileContents.contents.size());
	}
}
