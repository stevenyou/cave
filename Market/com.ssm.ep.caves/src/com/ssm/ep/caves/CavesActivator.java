package com.ssm.ep.caves;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;

import javax.activation.URLDataSource;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import com.ssm.ep.caves.editors.CavePropertiesFileContents;
import com.ssm.ep.caves.gitcommand.InitGIt;
import com.ssm.ep.caves.model.CavesManager;
import com.ssm.ep.caves.util.CheckInit;
import com.ssm.ep.caves.util.RunTimeGit;


/**
 * The activator class controls the plug-in life cycle
 */
public class CavesActivator extends AbstractUIPlugin {
	public static String name = "*";
	public static String email = "*";
	public static String url = "*";
	public static String dir = "*";
	public static String gdir = "*";
	public static String push = "*";
	public static String user = "*";
	public static String password = "*";
	public static String dummy1 = "C";
	public static String dummy2 = "C";
	
	// The plug-in ID
	public static final String PLUGIN_ID = "com.ssm.ep.caves";

	// The shared instance
	private static CavesActivator plugin;
	
	// The configuration preferences
	private IEclipsePreferences configPrefs;
	
	public CavesActivator() {
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		CavePropertiesFileContents.contents.clear();
		plugin = this;
		openGitSsh();
		
	}

	public void stop(BundleContext context) throws Exception {
      CavesManager.getManager().saveCaves();
        CavePropertiesFileContents.contents.clear();
		saveConfigPrefs();
		plugin = null;
		super.stop(context);
	}

	public static CavesActivator getDefault() {
		return plugin;
	}

	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	public File getConfigDir() {
		Location location = Platform.getConfigurationLocation();
		if (location != null) {
			URL configURL = location.getURL();
			if (configURL != null && configURL.getProtocol().startsWith("file")) {
				return new File(configURL.getFile(), PLUGIN_ID);
			}
		}
		return getStateLocation().toFile();
	}

	public Preferences getConfigPrefs() {
		if (configPrefs == null)
			configPrefs = new ConfigurationScope().getNode(PLUGIN_ID);
		return configPrefs;
	}
	
	public void saveConfigPrefs() {
		if (configPrefs != null) {
			try {
				configPrefs.flush();
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Version getVersion() {
		return new Version((String) getBundle().getHeaders().get(
				org.osgi.framework.Constants.BUNDLE_VERSION));
	}
	
	// "C:\Program Files (x86)\Git\bin\sh.exe" --login -i
		public void openGitSsh() {

			String PropFile = "C:\\lecture\\runtime-EclipseApplication\\.metadata\\.plugins\\org.eclipse.core.runtime"
					+ "\\.settings\\com.ssm.ep.caves.prefs";
			String propLine = null;

			try (BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(PropFile),
					System.getProperty("file.encoding")));) {
				while ((propLine = reader.readLine()) != null) {
					if (propLine.split("=")[0].equals("dir")) {
						dir = propLine.split("=")[1];
					} else if (propLine.split("=")[0].equals("name")) {
						name = propLine.split("=")[1];
					} else if (propLine.split("=")[0].equals("email")) {
						email = propLine.split("=")[1];
					} else if (propLine.split("=")[0].equals("url")) {
						url = propLine.split("=")[1];
					}  else if (propLine.split("=")[0].equals("user")) {
						user = propLine.split("=")[1];
					} else if (propLine.split("=")[0].equals("gdir")) {
						gdir = propLine.split("=")[1];
					} else if (propLine.split("=")[0].equals("push")) {
						push = propLine.split("=")[1];
						for(int i = 0; i < push.length(); i++){
							if(push.charAt(i) == 92){
								push = push.replace((push.charAt(3)+""),"");
							}
						}
						System.out.println(push.charAt(3));
						int d = push.charAt(3);
						System.out.println(d);
					} else if (propLine.split("=")[0].equals("password")) {
						password = propLine.split("=")[1];
					}
				}
				dummy1 = (dir.charAt(0)+"") + (dir.charAt(2)+"");
				dir = dir.split(":")[1];
				dummy2 = (gdir.charAt(0)+"") + (gdir.charAt(2)+"");
				
				// remove
				gdir = dummy2 + gdir.split(":")[1];
				System.out.println(" dummy1 : " + dummy1);
				System.out.println(" dummy2 : " + dummy2);
				System.out.println(" dir : " + dir);
				System.out.println(" email : " + email);
				System.out.println(" url : " + url);
				System.out.println(" name : " + name);
				System.out.println(" push : " + push);
				System.out.println(" gdir : " + gdir);
				System.out.println(" user : " + user);
				System.out.println(" password : " + password);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
//			Runtime runTime = Runtime.getRuntime();
//			Process process = null;
//			String cmd[] = new String[3];
//			cmd[0] = "cmd.exe";
//			cmd[1] = "/C";
//			cmd[2] = "cd " + dummy1 + dir + "&&sh.exe --login -i";
//			cmd[2] = "cd C:\\Program Files (x86)\\Git\\bin&&sh.exe --login -i";
			
			RunTimeGit rg = new RunTimeGit(dummy1, dir);
			CheckInit ck = new CheckInit();
			
			try {
				rg.process = rg.runTime.exec(rg.cmd);
				InitGIt so = new InitGIt(rg.process.getOutputStream(),name, email, url, gdir, ck.isContainList(gdir) );
				so.start();
				while (true) {
					if (!so.isAlive()) {
						rg.process.waitFor();
						break;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("실패....");
			} finally {
				if (rg.process != null)
					rg.process.destroy();
			}
		}
	
}
