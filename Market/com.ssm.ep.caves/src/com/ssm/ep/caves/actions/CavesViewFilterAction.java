package com.ssm.ep.caves.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IMemento;

import com.ssm.ep.caves.views.CavesViewNameFilter;


public class CavesViewFilterAction extends Action
{
   private final Shell shell;
   private final CavesViewNameFilter nameFilter;

   public CavesViewFilterAction(StructuredViewer viewer,
         String text) {
      super(text);
      shell = viewer.getControl().getShell();
      nameFilter = new CavesViewNameFilter(viewer);
   }

   public void run() {
      InputDialog dialog =
            new InputDialog(shell, "Caves View Filter",
                  "Enter a name filter pattern"
                        + " (* = any string, ? = any character)"
                        + System.getProperty("line.separator")
                        + "or an empty string for no filtering:",
                  nameFilter.getPattern(), null);
      if (dialog.open() == InputDialog.OK)
         nameFilter.setPattern(dialog.getValue().trim());
   }

   public void saveState(IMemento memento) {
      nameFilter.saveState(memento);
   }

   public void init(IMemento memento) {
      nameFilter.init(memento);
   }
}
