package com.ssm.ep.caves.editors;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.PathEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.ssm.ep.caves.CavesActivator;

public class CavePreferencePage extends FieldEditorPreferencePage 
								implements IWorkbenchPreferencePage {

	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(CavesActivator.getDefault().getPreferenceStore());
		
	}

	@Override
	protected void createFieldEditors() {
		
		
		// personal
		addField(new StringFieldEditor("name", "Name", getFieldEditorParent()));
		addField(new StringFieldEditor("email", "E-mail", getFieldEditorParent()));
		// user to ssh3344
		addField(new StringFieldEditor("user", "user for ssh", getFieldEditorParent()));
		// password to ssh3344
		addField(new StringFieldEditor("password", "password for ssh", getFieldEditorParent()));
		// Clone
		addField(new StringFieldEditor("url", "Clone url(to clone)", getFieldEditorParent()));
		// remote
		addField(new StringFieldEditor("push", "Remote url (to Push)", getFieldEditorParent()));
		addField(new DirectoryFieldEditor("dir", "Pick a Git Bach directory", getFieldEditorParent()));
		// init
		addField(new DirectoryFieldEditor("gdir", "Choose the git repository", getFieldEditorParent()));
		// targetProject
		String[][] data = null;
		String [] ids = null;
		IResource resouces[] = null;
		try {
			resouces = ResourcesPlugin.getWorkspace().getRoot().members();
		} catch (CoreException e1) {
			e1.printStackTrace();
		}
		ids = new String[resouces.length];
		for(int i = 0; i < resouces.length; i++){
			String projectName = resouces[i].toString().substring(resouces[i].toString().lastIndexOf("/")+1);
			ids[i] = projectName;
		}
		data = new String[ids.length][];
		for(int i = 0; i < ids.length; i++){
			data[i]= new String[] {ids[i], ids[i]};
		}
		addField(new ComboFieldEditor("tproj", "Choose the target Project", data , getFieldEditorParent()));
	}

}
