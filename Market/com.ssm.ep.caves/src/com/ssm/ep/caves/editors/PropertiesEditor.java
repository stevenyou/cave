package com.ssm.ep.caves.editors;

import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.core.commands.operations.ObjectUndoContext;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.contexts.IContextActivation;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.ide.IGotoMarker;
import org.eclipse.ui.operations.RedoActionHandler;
import org.eclipse.ui.operations.UndoActionHandler;
import org.eclipse.ui.part.MultiPageEditorPart;
import com.ssm.ep.caves.CavesLog;
import com.ssm.ep.caves.util.FileGenerater;

public class PropertiesEditor extends MultiPageEditorPart {

	public static final String VALUE_COLUMN_ID = "Value";
	public static final String KEY_COLUMN_ID = "Key";

	private TreeViewer treeViewer;
	private TextEditor textEditor;

	private TreeColumn keyColumn;
	private TreeColumn valueColumn;

	private PropertiesEditorContentProvider treeContentProvider;
	private PropertiesEditorLabelProvider treeLabelProvider;

	private boolean isPageModified;

	private UndoActionHandler undoAction;
	private RedoActionHandler redoAction;
	private IUndoContext undoContext;
	
	private final PropertyFileListener propertyFileListener = new PropertyFileListener() {
		public void keyChanged(PropertyCategory category, PropertyEntry entry) {
			treeViewer.refresh(entry);
			treeModified();
		}

		public void valueChanged(PropertyCategory category, PropertyEntry entry) {
			treeViewer.refresh(entry);
			treeModified();
		}

		public void nameChanged(PropertyCategory category) {
			treeViewer.refresh(category);
			treeModified();
		}

		public void entryAdded(PropertyCategory category, PropertyEntry entry) {
			treeViewer.refresh();
			treeModified();
		}

		public void entryRemoved(PropertyCategory category, PropertyEntry entry) {
			treeViewer.refresh();
			treeModified();
		}

		public void categoryAdded(PropertyCategory category) {
			treeViewer.refresh();
			treeModified();
		}

		public void categoryRemoved(PropertyCategory category) {
			treeViewer.refresh();
			treeModified();
		}
	};

	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		if (!(input instanceof IFileEditorInput))
			throw new PartInitException(
					"Invalid Input: Must be IFileEditorInput");
		super.init(site, input);
	}

	protected void createPages() {
		CavePropertiesFileContents.contents.clear();
		createPropertiesPage();
		createSourcePage();
		updateTitle();
		initTreeContent();
		initTreeEditors();
		createContextMenu();
		initKeyBindingContext();
		initUndoRedo();
	}

	void createPropertiesPage() {
		Composite treeContainer = new Composite(getContainer(), SWT.NONE);
		TreeColumnLayout layout = new TreeColumnLayout();
		treeContainer.setLayout(layout);

		treeViewer = new TreeViewer(treeContainer, SWT.MULTI
				| SWT.FULL_SELECTION);
		Tree tree = treeViewer.getTree();
		tree.setHeaderVisible(true);

		keyColumn = new TreeColumn(tree, SWT.NONE);
		keyColumn.setText("Key");
		layout.setColumnData(keyColumn, new ColumnWeightData(2));

		valueColumn = new TreeColumn(tree, SWT.NONE);
		valueColumn.setText("Value");
		layout.setColumnData(valueColumn, new ColumnWeightData(3));

		int index = addPage(treeContainer);
		setPageText(index, "Properties");
		getSite().setSelectionProvider(treeViewer);
	}

	void createSourcePage() {
		try {
			textEditor = new TextEditor();
			int index = addPage(textEditor, getEditorInput());
			setPageText(index, "Source");
		} catch (PartInitException e) {
			CavesLog.logError("Error creating nested text editor", e);
		}
	}

	void updateTitle() {
		IEditorInput input = getEditorInput();
		setPartName(input.getName());
		setTitleToolTip(input.getToolTipText());
	}

	void initTreeContent() {
		treeContentProvider = new PropertiesEditorContentProvider();
		treeViewer.setContentProvider(treeContentProvider);
		treeLabelProvider = new PropertiesEditorLabelProvider();
		treeViewer.setLabelProvider(treeLabelProvider);

		// Reset the input from the text editor�s content
		// after the editor initialization has completed.
		treeViewer.setInput(new PropertyFile(""));
		treeViewer.getTree().getDisplay().asyncExec(new Runnable() {
			public void run() {
				updateTreeFromTextEditor();
			}
		});
		treeViewer.setAutoExpandLevel(TreeViewer.ALL_LEVELS);
	}

	private void initTreeEditors() {

		treeViewer.setColumnProperties(new String[] { KEY_COLUMN_ID,
				VALUE_COLUMN_ID });
		final TextCellEditor keyEditor = new TextCellEditor(
				treeViewer.getTree());
		final TextCellEditor valueEditor = new TextCellEditor(
				treeViewer.getTree());
		treeViewer.setCellEditors(new CellEditor[] { keyEditor, valueEditor });

//		treeViewer.setCellModifier(new PropertiesEditorCellModifier(this,
//				treeViewer));

		// devide

		// TreeViewerColumn column1 = new TreeViewerColumn(treeViewer,
		// keyColumn);
		// TreeViewerColumn column2 = new TreeViewerColumn(treeViewer,
		// valueColumn);
		//
		// column1.setLabelProvider(new ColumnLabelProvider() {
		// public String getText(Object element) {
		// return treeLabelProvider.getColumnText(element, 0);
		// }
		// });
		// column2.setLabelProvider(new ColumnLabelProvider() {
		// public String getText(Object element) {
		// return treeLabelProvider.getColumnText(element, 1);
		// }
		// });
		//
		// column1.setEditingSupport(new EditingSupport(treeViewer) {
		// TextCellEditor editor = null;
		//
		// protected boolean canEdit(Object element) {
		// return true;
		// }
		//
		// protected CellEditor getCellEditor(Object element) {
		// if (editor == null) {
		// Composite tree = (Composite) treeViewer.getControl();
		// editor = new TextCellEditor(tree);
		// editor.setValidator(new ICellEditorValidator() {
		// public String isValid(Object value) {
		// if (((String) value).trim().length() == 0)
		// return "Key must not be empty string";
		// return null;
		// }
		// });
		// editor.addListener(new ICellEditorListener() {
		// public void applyEditorValue() {
		// setErrorMessage(null);
		// }
		//
		// public void cancelEditor() {
		// setErrorMessage(null);
		// }
		//
		// public void editorValueChanged(boolean oldValidState,
		// boolean newValidState) {
		// setErrorMessage(editor.getErrorMessage());
		// }
		//
		// private void setErrorMessage(String errorMessage) {
		// getEditorSite().getActionBars()
		// .getStatusLineManager()
		// .setErrorMessage(errorMessage);
		// }
		// });
		// }
		// return editor;
		// }
		//
		// protected Object getValue(Object element) {
		// return treeLabelProvider.getColumnText(element, 0);
		// }
		//
		// protected void setValue(Object element, Object value) {
		// if (value == null)
		// return;
		// String text = ((String) value).trim();
		// if (element instanceof PropertyCategory)
		// ((PropertyCategory) element).setName(text);
		// if (element instanceof PropertyEntry)
		// ((PropertyEntry) element).setKey(text);
		// }
		// });
		//
		// column2.setEditingSupport(new EditingSupport(treeViewer) {
		// TextCellEditor editor = null;
		//
		// protected boolean canEdit(Object element) {
		// return element instanceof PropertyEntry;
		// }
		//
		// protected CellEditor getCellEditor(Object element) {
		// if (editor == null) {
		// Composite tree = (Composite) treeViewer.getControl();
		// editor = new TextCellEditor(tree);
		// }
		// return editor;
		// }
		//
		// protected Object getValue(Object element) {
		// return treeLabelProvider.getColumnText(element, 1);
		// }
		//
		// protected void setValue(Object element, Object value) {
		// String text = ((String) value).trim();
		// if (element instanceof PropertyEntry)
		// ((PropertyEntry) element).setValue(text);
		// }
		// });
		//
		// treeViewer.getColumnViewerEditor().addEditorActivationListener(
		// new AltClickCellEditListener());
		//

	}

	private void createContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager m) {
				PropertiesEditor.this.fillContextMenu(m);
			}
		});
		Tree tree = treeViewer.getTree();
		Menu menu = menuMgr.createContextMenu(tree);
		tree.setMenu(menu);
		getSite().registerContextMenu(menuMgr, treeViewer);
	}

	private void fillContextMenu(IMenuManager menuMgr) {
		menuMgr.add(undoAction);
		menuMgr.add(redoAction);
		menuMgr.add(new Separator("edit"));
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void initKeyBindingContext() {
		final IContextService service = (IContextService) getSite().getService(
				IContextService.class);

		treeViewer.getControl().addFocusListener(new FocusListener() {
			IContextActivation currentContext = null;

			public void focusGained(FocusEvent e) {
				if (currentContext == null)
					currentContext = service
							.activateContext("com.ssm.ep.caves.properties.editor.context");
			}

			public void focusLost(FocusEvent e) {
				if (currentContext != null)
					service.deactivateContext(currentContext);
			}
		});
	}

	private void initUndoRedo() {
		undoContext = new ObjectUndoContext(this);
		undoAction = new UndoActionHandler(getSite(), undoContext);
		redoAction = new RedoActionHandler(getSite(), undoContext);
	}

	private void setTreeUndoRedo() {
		final IActionBars actionBars = getEditorSite().getActionBars();
		actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(),
				undoAction);
		actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(),
				redoAction);
		actionBars.updateActionBars();
	}

	private void setTextEditorUndoRedo() {
		final IActionBars actionBars = getEditorSite().getActionBars();
		IAction undoAction2 = textEditor.getAction(ActionFactory.UNDO.getId());
		actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(),
				undoAction2);
		IAction redoAction2 = textEditor.getAction(ActionFactory.REDO.getId());
		actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(),
				redoAction2);
		actionBars.updateActionBars();
		getOperationHistory().dispose(undoContext, true, true, false);
	}

	void updateTreeFromTextEditor() {
		PropertyFile propertyFile = (PropertyFile) treeViewer.getInput();
		propertyFile.removePropertyFileListener(propertyFileListener);
		propertyFile = new PropertyFile(textEditor.getDocumentProvider()
				.getDocument(textEditor.getEditorInput()).get());

		treeViewer.setInput(propertyFile);
		propertyFile.addPropertyFileListener(propertyFileListener);
	}

	void updateTextEditorFromTree() {
		textEditor.getDocumentProvider()
				.getDocument(textEditor.getEditorInput())
				.set(((PropertyFile) treeViewer.getInput()).asText());
	}

	public void treeModified() {
		boolean wasDirty = isDirty();
		isPageModified = true;
		if (!wasDirty)
			firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	protected void handlePropertyChange(int propertyId) {
		if (propertyId == IEditorPart.PROP_DIRTY)
			isPageModified = isDirty();
		super.handlePropertyChange(propertyId);
	}

	public boolean isDirty() {
		return isPageModified || super.isDirty();
	}

	protected void pageChange(int newPageIndex) {
		switch (newPageIndex) {
		case 0:
			if (isDirty())
				updateTreeFromTextEditor();
			setTreeUndoRedo();
			break;
		case 1:
			if (isPageModified)
				updateTextEditorFromTree();
			setTextEditorUndoRedo();
			break;
		}
		isPageModified = false;

		super.pageChange(newPageIndex);
	}

	public void setFocus() {
		switch (getActivePage()) {
		case 0:
			treeViewer.getTree().setFocus();
			break;
		case 1:
			textEditor.setFocus();
			break;
		}
	}

	public void gotoMarker(IMarker marker) {
		setActivePage(1);
		((IGotoMarker) getAdapter(IGotoMarker.class)).gotoMarker(marker);
	}

	public boolean isSaveAsAllowed() {
		return true;
	}

	public void doSave(IProgressMonitor monitor) {
		System.out.println("@@@ doSave");
		if (getActivePage() == 0 && isPageModified)
			updateTextEditorFromTree();
		isPageModified = false;
		textEditor.doSave(monitor);
		FileGenerater.makeFile();
		initTreeContent();
		// here
	}

	public void doSaveAs() {
		if (getActivePage() == 0 && isPageModified)
			updateTextEditorFromTree();
		System.out.println("@@@ doSaveAs");
		isPageModified = false;
		textEditor.doSaveAs();
		FileGenerater.makeFile();
		setInput(textEditor.getEditorInput());
		updateTitle();
	}

	public IOperationHistory getOperationHistory() {

		// The workbench provides its own undo/redo manager
		// return PlatformUI.getWorkbench()
		// .getOperationSupport().getOperationHistory();

		// which, in this case, is the same as the default undo manager
		return OperationHistoryFactory.getOperationHistory();
	}

	public IUndoContext getUndoContext() {

		// For workbench-wide operations, we should return
		// return PlatformUI.getWorkbench()
		// .getOperationSupport().getUndoContext();

		// but our operations are all local, so return our own content
		return undoContext;
	}
}
