//package com.ssm.ep.caves.editors;
//
//import org.eclipse.jface.viewers.ICellModifier;
//import org.eclipse.jface.viewers.TreeViewer;
//import org.eclipse.swt.widgets.TreeItem;
//
//public class PropertiesEditorCellModifier implements ICellModifier {
//	
//	private PropertiesEditor editor;
//
//	public PropertiesEditorCellModifier(PropertiesEditor editor,
//			TreeViewer viewer) {
//		this.editor = editor;
//	}
//
//	@Override
//	public boolean canModify(Object element, String property) {
//		if(property == PropertiesEditor.KEY_COLUMN_ID){
//			if(element instanceof PropertyCategory)
//				return true;
//			if(element instanceof PropertyEntry)
//				return true;
//		}
//		if(property == PropertiesEditor.VALUE_COLUMN_ID){
//			if(element instanceof PropertyEntry)
//				return true;
//		}
//		return false;
//	}
//
//	@Override
//	public Object getValue(Object element, String property) {
//		if(property == PropertiesEditor.KEY_COLUMN_ID){
//			if(element instanceof PropertyCategory)
//				return ((PropertyCategory)element).getName();
//			if(element instanceof PropertyEntry)
//				return ((PropertyEntry)element).getKey();
//		}
//		if(property == PropertiesEditor.VALUE_COLUMN_ID){
//			if(element instanceof PropertyEntry)
//				return ((PropertyEntry)element).getValue();
//		}
//		return null;
//	}
//
//	@Override
//	public void modify(Object item, String property, Object value) {
//		if(value == null) return;
//		
//		Object element = item;
//		if(element instanceof TreeItem)
//			element = ((TreeItem)element).getData();
//		
//		String text = ((String)value).trim();
//		if(property == PropertiesEditor.KEY_COLUMN_ID){
//			if(element instanceof PropertyCategory)
//				((PropertyCategory)element).setName(text);
//			if(element instanceof PropertyEntry)
//				((PropertyEntry)element).setKey(text);
//		}
//		if(property == PropertiesEditor.VALUE_COLUMN_ID){
//			if(element instanceof PropertyEntry)
//				((PropertyEntry)element).setValue(text);
//		}
//	}
//
//}
