package com.ssm.ep.caves.editors;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.ssm.ep.caves.util.pakageName;

public class PropertyCategory extends PropertyElement {
	private String name;
	private List<PropertyEntry> entries;
	private int c;
	private String filename;
	private String packagename;
	private String includename;
	public List<String> contents = new ArrayList<String>();
	
	public PropertyCategory(PropertyFile parent, LineNumberReader reader)
			throws IOException {
		super(parent);
		// Determine the category name from comments.
		while (true) {
			reader.mark(1);
			reader.skip(3);
			int ch = reader.read();
			if (ch == -1)
				break;
			reader.reset();
			if (ch != '#')
				break;
			String line = reader.readLine();
			line = line.replace(" ", "");
			if (line.contains("///")) {
				line = line.replace("///", "");
				if (name == null) {
					line = line.replace('#', ' ').trim();
					if (line.length() > 0) {
						name = line;
						CavePropertiesFileContents.contents.add("funcname="
								+ line);
					}
				}
			}
		}
		if (name == null)
			name = "";

		// Determine the properties in this category.
		entries = new ArrayList<PropertyEntry>();
		while (true) {
			reader.mark(1);
			reader.skip(3);
			int ch = reader.read();
			if (ch == -1)
				break;
			reader.reset();
			if (ch == '#')
				break;
			String line = reader.readLine();
			if (line.startsWith("package") && !line.contains("///")) {
				packagename = line.replace("package ","");
				packagename = packagename.replace(";", "");
				System.out.println("&& packagename : " + packagename);
				CavePropertiesFileContents.contents.add("packagename=" + packagename);
			}
			if (line.startsWith("#include") && !line.contains("///")) {
				includename = line.replace("#include ","");
				includename = includename.replace("<", "");
				includename = includename.replace(">", "");
				System.out.println("&& includename : " + includename);
				CavePropertiesFileContents.contents.add("include=" + includename);
				CavePropertiesFileContents.contents.add("packagename=" + "");
			}
			line = line.replace(" ", "");
			System.out.println("line : " + line);
			
			if (line.startsWith("filename") && line.contains("///")) {
				filename = line.replace("filename ","");
				filename = filename.replace("=", "");
				System.out.println("&& FileName : " + filename);
				CavePropertiesFileContents.contents.add("filename=" + filename);
			}
			
			
			if (line.contains("class") && !line.contains("///")) {
				if (line.contains("extends")) {
					int index = line.lastIndexOf("class") + 4;
					int index2 = line.indexOf("extends");
					filename = line.substring(index + 1, index2).trim();
				} else if (line.contains("implements")) {
					int index = line.lastIndexOf("class") + 4;
					int index2 = line.indexOf("implements");
					filename = line.substring(index + 1, index2).trim();
				} else if (line.contains("{")) {
					int index = line.lastIndexOf("class") + 4;
					int index2 = line.indexOf('{');
					filename = line.substring(index + 1, index2).trim();
				} else {
					int index = line.lastIndexOf("class") + 4;
					filename = line.substring(index + 1).trim();
				}

				System.out.println("&& FileName : " + filename);
				CavePropertiesFileContents.contents.add("filename=" + filename);

			}
			
			if(line.contains("import") && !line.contains("///")){
				line = line.replace("import", "");
				boolean cont = true;
				for(int i = 0; i < pakageName.pkgs.length; i++ ){
					if(line.startsWith(pakageName.pkgs[i])){
						cont = false;
					}
				}
				if(cont){
					System.out.println("import : " + line);
					CavePropertiesFileContents.contents.add("import=" + line);
				}
			}

			if ((line.contains("newFileReader") && !line.contains("import"))
					|| (line.contains("newFileInputStream") && !line
							.contains("import"))) {
				int index = line.lastIndexOf('(');
				int index2 = line.indexOf(')');
				System.out.println(" * in this : " + line);
				if (index != -1) {
					String key = "file";
					String value = line.substring(index + 1, index2).trim();
					System.out.println("key : " + key + " , value " + value);
					CavePropertiesFileContents.contents.add(key + "=" + value);
					entries.add(new PropertyEntry(this, key, value));
				}
			}
			if (line.contains("///")) {
				line = line.replace("///", "");
				int index = line.indexOf('=');
				if (index != -1) {
					String key = line.substring(0, index).trim();
					String value = line.substring(index + 1).trim();
					System.out.println("key : " + key + " , value " + value);
					CavePropertiesFileContents.contents.add(key + "=" + value);
					entries.add(new PropertyEntry(this, key, value));
				}
			}
		}
	}

	public String getName() {
		return name;
	}

	public Collection<PropertyEntry> getEntries() {
		return entries;
	}

	public PropertyElement[] getChildren() {
		return (PropertyElement[]) entries.toArray(new PropertyElement[entries
				.size()]);
	}

	public void setName(String text) {
		if (name.equals(text))
			return;
		name = text;
		((PropertyFile) getParent()).nameChanged(this);
	}

	public void addEntry(PropertyEntry entry) {
		addEntry(entries.size(), entry);
	}

	public void addEntry(int index, PropertyEntry entry) {
		if (!entries.contains(entry)) {
			entries.add(index, entry);
			((PropertyFile) getParent()).entryAdded(this, entry);
		}
	}

	public void removeEntry(PropertyEntry entry) {
		if (entries.remove(entry))
			((PropertyFile) getParent()).entryRemoved(this, entry);
	}

	public void removeFromParent() {
		((PropertyFile) getParent()).removeCategory(this);
	}

	public void keyChanged(PropertyEntry entry) {
		((PropertyFile) getParent()).keyChanged(this, entry);
	}

	public void valueChanged(PropertyEntry entry) {
		((PropertyFile) getParent()).valueChanged(this, entry);
	}

	public void appendText(PrintWriter writer) {
		if (name.length() > 0) {
			writer.print("# ");
			writer.println(name);
		}
		Iterator<PropertyEntry> iter = entries.iterator();
		while (iter.hasNext())
			iter.next().appendText(writer);
	}
}
