package com.ssm.ep.caves.views;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TransferData;

import com.ssm.ep.caves.model.CavesManager;
import com.ssm.ep.caves.model.ICaveItem;

public class CavesTransfer extends ByteArrayTransfer
{
   private static final CavesTransfer INSTANCE =
         new CavesTransfer();

   private static final String TYPE_NAME =
         "Caves-transfer-format:" + System.currentTimeMillis()
               + ":" + INSTANCE.hashCode();

   private static final int TYPEID = registerType(TYPE_NAME);

   private CavesTransfer() {
      super();
   }
   public static CavesTransfer getInstance() {
      return INSTANCE;
   }
   protected int[] getTypeIds() {
      return new int[] { TYPEID };
   }

   protected String[] getTypeNames() {
      return new String[] { TYPE_NAME };
   }

   protected void javaToNative(Object data, TransferData transferData) {

      if (!(data instanceof ICaveItem[]))
         return;
      ICaveItem[] items = (ICaveItem[]) data;


      try {
         ByteArrayOutputStream out = new ByteArrayOutputStream();
         DataOutputStream dataOut = new DataOutputStream(out);
         dataOut.writeInt(items.length);
         for (int i = 0; i < items.length; i++) {
            ICaveItem item = items[i];
            dataOut.writeUTF(item.getType().getId());
            dataOut.writeUTF(item.getInfo());
         }
         dataOut.close();
         out.close();
         super.javaToNative(out.toByteArray(), transferData);
      }
      catch (IOException e) {
         // Send nothing if there were problems.
      }
   }
   protected Object nativeToJava(TransferData transferData) {


      byte[] bytes = (byte[]) super.nativeToJava(transferData);
      if (bytes == null)
         return null;
      DataInputStream in =
            new DataInputStream(new ByteArrayInputStream(bytes));
      try {
         CavesManager mgr = CavesManager.getManager();
         int count = in.readInt();
         List<ICaveItem> items =
               new ArrayList<ICaveItem>(count);
         for (int i = 0; i < count; i++) {
            String typeId = in.readUTF();
            String info = in.readUTF();
            items.add(mgr.newCaveFor(typeId, info));
         }
         return items.toArray(new ICaveItem[items.size()]);
      }
      catch (IOException e) {
         return null;
      }
   }
}