package com.ssm.ep.caves.views;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;

import com.ssm.ep.caves.model.CavesManager;
import com.ssm.ep.caves.model.CavesManagerEvent;
import com.ssm.ep.caves.model.CavesManagerListener;


// 컨택트 프로바이더는 입력 객체(CavesManager)로부터 출력할 객체들을 꺼내고,
// 출력용 테이블 뷰어로 한 행 마다 객체 하나씩을 넘기는 역할을 수행한다.
// CavesManager의 내용이 변경 되어있을때 뷰어를 갱신시킬 책임이 있다.

public class CavesViewContentProvider implements IStructuredContentProvider,
		CavesManagerListener {

	private TableViewer viewer;
	private CavesManager manager;

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this.viewer = (TableViewer) viewer;
		if (manager != null)
			manager.removeCavesManagerListener(this);
		manager = (CavesManager) newInput;
		if (manager != null)
			manager.addCavesManagerListener(this);
	}
	
	public void dispose() {
	}

	public Object[] getElements(Object parent) {
		return manager.getCaves();
	}
	
	// 다시 그릴때 깜빡이는 현상을 줄이기 위해 
	// setRedraw 함수 사용
	public void CavesChanged(CavesManagerEvent event) {
		viewer.getTable().setRedraw(false);
		try {
			viewer.remove(event.getItemsRemoved());
			viewer.add(event.getItemsAdded());
		} finally {
			viewer.getTable().setRedraw(true);
		}
	}

	@Override
	public void cavesChanged(CavesManagerEvent event) {
		viewer.getTable().setRedraw(false);
		try {
			viewer.remove(event.getItemsRemoved());
			viewer.add(event.getItemsAdded());
		} finally {
			viewer.getTable().setRedraw(true);
		}
	}

}
