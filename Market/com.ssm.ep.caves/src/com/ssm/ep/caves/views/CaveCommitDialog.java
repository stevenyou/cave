package com.ssm.ep.caves.views;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Shell;

public class CaveCommitDialog extends MessageDialog{
	
	
	public CaveCommitDialog(Shell parentShell, int changed) {
		super(parentShell, "Commit", null, changed + "'s Cave Source Commited to Rit",
				INFORMATION, new String[] {IDialogConstants.OK_LABEL}, 0);
	}
	
}
