package com.ssm.ep.caves.views;

import java.net.MalformedURLException;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.ssm.ep.caves.model.ICaveItem;

public class CavesViewLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	@Override
	public Image getColumnImage(Object obj, int index) {
		if ((index == 0) && (obj instanceof ICaveItem))
			return ((ICaveItem) obj).getType().getImage();
		return null;
	}

	@Override
	public String getColumnText(Object obj, int index) {
		switch (index) {
		case 0: // Type column
			return "";
		case 1: // Name column
			if (obj instanceof ICaveItem)
				return ((ICaveItem) obj).getName();
			if (obj != null)
				return obj.toString();
			return "";
		case 2: // Location column
			if (obj instanceof ICaveItem)
				return ((ICaveItem) obj).getLocation();
			return "";
		case 3: // Modification column
			if (obj instanceof ICaveItem)
				try {
					return ((ICaveItem) obj).getMod();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return "";
		case 4: // File Size column
			if (obj instanceof ICaveItem)
				return ((ICaveItem) obj).getFileSize();
			return "";
		case 5: // Rit State column
			if (obj instanceof ICaveItem)
				return ((ICaveItem) obj).getRit();
			return "";
		default:
			return "";
		}	}

}
