package com.ssm.ep.caves.views;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.Comparator;

import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationListener;
import org.eclipse.jface.viewers.ColumnViewerEditorDeactivationEvent;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.IHandlerActivation;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.IWorkbenchActionDefinitionIds;

import com.ssm.ep.caves.actions.CavesViewFilterAction;
import com.ssm.ep.caves.contributions.RemoveCavesContributionItem;
import com.ssm.ep.caves.gitcommand.InitGIt;
import com.ssm.ep.caves.handlers.RemoveCavesHandler;
import com.ssm.ep.caves.handlers.RenameCavesHandler;
import com.ssm.ep.caves.model.CavesManager;
import com.ssm.ep.caves.model.ICaveItem;
import com.ssm.ep.caves.util.EditorUtil;
import com.ssm.ep.caves.util.StreamGobbler;

public class CavesView extends ViewPart {

	public static final String ID = "com.ssm.ep.caves.views.CavesView";

	private TableViewer viewer;
	private TableColumn typeColumn;
	private TableColumn nameColumn;
	private TableColumn locationColumn;
	private TableColumn modifiColumn;
	private TableColumn fileSizeColumn;
	private TableColumn ritStateColumn;

	private CavesViewSorter sorter;

	private CavesViewFilterAction filterAction;

	private IHandler removeHandler;
	private RemoveCavesContributionItem removeContributionItem;

	private ISelectionListener pageSelectionListener;

	private IMemento memento;

	public CavesView() {
	}

	// 뷰를 구성하는 컨트롤들을 생성 ------> module화된 method들을 내부적으로 호출
	public void createPartControl(Composite parent) {
		createTableViewer(parent);
		createTableSorter();
		createContributions();
		createContextMenu();
		createToolbarButtons();
		createViewPulldownMenu();
		hookKeyboard();
		hookGlobalHandlers();
		hookDragAndDrop();
		createInlineEditor();
		hookMouse();
		hookPageSelection();
	}

	private void createTableViewer(Composite parent) {
		viewer = new TableViewer(parent, SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.MULTI | SWT.FULL_SELECTION);
		// FULL_SELECTION ----> 한행전체 선택 트,ㅡ,
		final Table table = viewer.getTable();

		TableColumnLayout layout = new TableColumnLayout();
		parent.setLayout(layout);

		typeColumn = new TableColumn(table, SWT.LEFT);
		typeColumn.setText("");
		layout.setColumnData(typeColumn, new ColumnPixelData(18));

		nameColumn = new TableColumn(table, SWT.LEFT);
		nameColumn.setText("Name");
		layout.setColumnData(nameColumn, new ColumnWeightData(3));

		locationColumn = new TableColumn(table, SWT.LEFT);
		locationColumn.setText("Location");
		layout.setColumnData(locationColumn, new ColumnWeightData(7));

		modifiColumn = new TableColumn(table, SWT.LEFT);
		modifiColumn.setText("Last Mod");
		layout.setColumnData(modifiColumn, new ColumnWeightData(5));

		fileSizeColumn = new TableColumn(table, SWT.LEFT);
		fileSizeColumn.setText("File Size");
		layout.setColumnData(fileSizeColumn, new ColumnWeightData(3));

		ritStateColumn = new TableColumn(table, SWT.LEFT);
		ritStateColumn.setText("State");
		layout.setColumnData(ritStateColumn, new ColumnWeightData(3));

		table.setHeaderVisible(true);
		table.setLinesVisible(false);

		viewer.setContentProvider(new CavesViewContentProvider());
		viewer.setLabelProvider(new CavesViewLabelProvider());
		viewer.setInput(CavesManager.getManager());

		getSite().setSelectionProvider(viewer);
	}

	private void createTableSorter() {
		Comparator<ICaveItem> nameComparator = new Comparator<ICaveItem>() {
			public int compare(ICaveItem i1, ICaveItem i2) {
				return i1.getName().compareTo(i2.getName());
			}
		};
		Comparator<ICaveItem> locationComparator = new Comparator<ICaveItem>() {
			public int compare(ICaveItem i1, ICaveItem i2) {
				return i1.getLocation().compareTo(i2.getLocation());
			}
		};
		Comparator<ICaveItem> typeComparator = new Comparator<ICaveItem>() {
			public int compare(ICaveItem i1, ICaveItem i2) {
				return i1.getType().compareTo(i2.getType());
			}
		};
		Comparator<ICaveItem> modComparator = new Comparator<ICaveItem>() {
			public int compare(ICaveItem i1, ICaveItem i2) {
				return i1.getType().compareTo(i2.getType());
			}
		};
		Comparator<ICaveItem> fsizeComparator = new Comparator<ICaveItem>() {
			public int compare(ICaveItem i1, ICaveItem i2) {
				return i1.getType().compareTo(i2.getType());
			}
		};
		Comparator<ICaveItem> ritStComparator = new Comparator<ICaveItem>() {
			public int compare(ICaveItem i1, ICaveItem i2) {
				return i1.getType().compareTo(i2.getType());
			}
		};
		sorter = new CavesViewSorter(viewer, new TableColumn[] { nameColumn,
				locationColumn, typeColumn, modifiColumn, fileSizeColumn,
				ritStateColumn }, new Comparator[] { nameComparator,
				locationComparator, typeComparator, modComparator,
				fsizeComparator, ritStComparator });
		if (memento != null)
			sorter.init(memento);
		viewer.setSorter(sorter);
	}

	private void createContributions() {
		removeHandler = new RemoveCavesHandler();
		removeContributionItem = new RemoveCavesContributionItem(getViewSite(),
				removeHandler);
	}

	private void createContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(IMenuManager manager) {
				CavesView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager menuMgr) {
		menuMgr.add(new Separator("edit"));
		menuMgr.add(removeContributionItem);
		menuMgr.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void createToolbarButtons() {
		IToolBarManager toolBarMgr = getViewSite().getActionBars()
				.getToolBarManager();
		toolBarMgr.add(new GroupMarker("edit"));
		toolBarMgr.add(removeContributionItem);
	}

	private void createViewPulldownMenu() {
		IMenuManager menu = getViewSite().getActionBars().getMenuManager();
		filterAction = new CavesViewFilterAction(viewer, "Filter...");
		if (memento != null)
			filterAction.init(memento);
		menu.add(filterAction);
	}

	private void hookKeyboard() {
		viewer.getControl().addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent event) {
				handleKeyReleased(event);
			}
		});
	}

	protected void handleKeyReleased(KeyEvent event) {
		if (event.keyCode == SWT.F2 && event.stateMask == 0) {
			new RenameCavesHandler().editElement(this);
		}
		if (event.character == SWT.DEL && event.stateMask == 0) {
			removeContributionItem.run();
		}
	}

	private void hookGlobalHandlers() {
		// getViewSite().getActionBars().setGlobalActionHandler(
		// ActionFactory.CUT.getId(), cutAction);
		// getViewSite().getActionBars().setGlobalActionHandler(
		// ActionFactory.COPY.getId(), copyAction);
		// getViewSite().getActionBars().setGlobalActionHandler(
		// ActionFactory.PASTE.getId(), pasteAction);

		final IHandlerService handlerService = (IHandlerService) getViewSite()
				.getService(IHandlerService.class);
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			private IHandlerActivation removeActivation;

			public void selectionChanged(SelectionChangedEvent event) {
				if (event.getSelection().isEmpty()) {
					if (removeActivation != null) {
						handlerService.deactivateHandler(removeActivation);
						removeActivation = null;
					}
				} else {
					if (removeActivation == null) {
						removeActivation = handlerService.activateHandler(
								IWorkbenchActionDefinitionIds.DELETE,
								removeHandler);
					}
				}
			}
		});
	}

	private void hookDragAndDrop() {
		new CavesDragSource(viewer);
		new CavesDropTarget(viewer);
	}

	private void hookMouse() {
		viewer.getTable().addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent e) {
				EditorUtil.openEditor(getSite().getPage(),
						viewer.getSelection());
			}
		});
	}

	private void hookPageSelection() {
		pageSelectionListener = new ISelectionListener() {
			public void selectionChanged(IWorkbenchPart part,
					ISelection selection) {
				pageSelectionChanged(part, selection);
			}
		};
		getSite().getPage().addPostSelectionListener(pageSelectionListener);
	}

	protected void pageSelectionChanged(IWorkbenchPart part,
			ISelection selection) {
		if (part == this)
			return;
		if (!(selection instanceof IStructuredSelection))
			return;
		IStructuredSelection sel = (IStructuredSelection) selection;
		ICaveItem[] items = CavesManager.getManager().existingCavesFor(
				sel.iterator());
		if (items.length > 0)
			viewer.setSelection(new StructuredSelection(items), true);
	}

	private void createInlineEditor() {
		TableViewerColumn column = new TableViewerColumn(viewer, nameColumn);

		column.setLabelProvider(new ColumnLabelProvider() {
			public String getText(Object element) {
				return ((ICaveItem) element).getName();
			}
		});

		column.setEditingSupport(new EditingSupport(viewer) {
			TextCellEditor editor = null;

			protected boolean canEdit(Object element) {
				return true;
			}

			protected CellEditor getCellEditor(Object element) {
				if (editor == null) {
					Composite table = (Composite) viewer.getControl();
					editor = new TextCellEditor(table);
				}
				return editor;
			}

			protected Object getValue(Object element) {
				return ((ICaveItem) element).getName();
			}

			protected void setValue(Object element, Object value) {
				((ICaveItem) element).setName((String) value);
				viewer.refresh(element);
			}
		});

		viewer.getColumnViewerEditor().addEditorActivationListener(
				new ColumnViewerEditorActivationListener() {
					public void beforeEditorActivated(
							ColumnViewerEditorActivationEvent event) {
						if (event.eventType == ColumnViewerEditorActivationEvent.MOUSE_CLICK_SELECTION) {
							if (!(event.sourceEvent instanceof MouseEvent))
								event.cancel = true;
							else {
								MouseEvent mouseEvent = (MouseEvent) event.sourceEvent;
								if ((mouseEvent.stateMask & SWT.ALT) == 0)
									event.cancel = true;
							}
						} else if (event.eventType != ColumnViewerEditorActivationEvent.PROGRAMMATIC)
							event.cancel = true;
					}

					public void afterEditorActivated(
							ColumnViewerEditorActivationEvent event) {
					}

					public void beforeEditorDeactivated(
							ColumnViewerEditorDeactivationEvent event) {
					}

					public void afterEditorDeactivated(
							ColumnViewerEditorDeactivationEvent event) {
					}
				});
	}

	

	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public TableViewer getCavesViewer() {
		return viewer;
	}

	public void dispose() {
		if (pageSelectionListener != null)
			getSite().getPage().removePostSelectionListener(
					pageSelectionListener);
		super.dispose();
	}

	public void saveState(IMemento memento) {
		super.saveState(memento);
		sorter.saveState(memento);
		filterAction.saveState(memento);
	}

	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		this.memento = memento;
	}

	public IStructuredSelection getSelection() {
		return (IStructuredSelection) viewer.getSelection();
	}

	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		viewer.addSelectionChangedListener(listener);
	}
	
}

