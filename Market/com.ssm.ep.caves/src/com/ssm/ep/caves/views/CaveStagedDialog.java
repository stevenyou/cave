package com.ssm.ep.caves.views;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Shell;

public class CaveStagedDialog extends MessageDialog{
	
	
	public CaveStagedDialog(Shell parentShell, int changed) {
		super(parentShell, "Staged", null, changed + "'s Cave Source Staged to Rit",
				INFORMATION, new String[] {IDialogConstants.OK_LABEL}, 0);
	}
	
}
