package com.ssm.ep.caves.views;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

public class CavePushDialog extends MessageDialog{
	
	public CavePushDialog(Shell parentShell, int changed) {
		super(parentShell, "Pushed", null, changed + "'s Cave Source Pushed to Remote Rit Server",
				INFORMATION, new String[] {IDialogConstants.OK_LABEL}, 0);
	}
	
}
