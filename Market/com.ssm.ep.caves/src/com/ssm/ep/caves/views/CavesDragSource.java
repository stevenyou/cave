package com.ssm.ep.caves.views;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.part.ResourceTransfer;

import com.ssm.ep.caves.handlers.CopyCavesHandler;
public class CavesDragSource
      implements DragSourceListener
{
   private final TableViewer viewer;

   public CavesDragSource(TableViewer viewer) {
      this.viewer = viewer;
      DragSource source =
            new DragSource(viewer.getControl(), DND.DROP_COPY);
      source.setTransfer(new Transfer[] {
            TextTransfer.getInstance(),
            ResourceTransfer.getInstance() });
      source.addDragListener(this);
   }

   public void dragStart(DragSourceEvent event) {
      event.doit = !viewer.getSelection().isEmpty();
   }

   public void dragSetData(DragSourceEvent event) {
      Object[] objects =
            ((IStructuredSelection) viewer.getSelection()).toArray();
      if (ResourceTransfer.getInstance().isSupportedType(
            event.dataType)) {
         event.data = CopyCavesHandler.asResources(objects);
      }
      else if (TextTransfer.getInstance().isSupportedType(
            event.dataType)) {
         event.data = CopyCavesHandler.asText(objects);
      }
   }

   public void dragFinished(DragSourceEvent event) {
      // If this was a MOVE operation, 
      // then remove the items that were moved.
   }
}
