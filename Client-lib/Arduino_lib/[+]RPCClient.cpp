#include "RPCClient.h"
#include "w5100.h"
#include "socket.h"
#include "Ethernet.h"
#include <Udp.h>
#include "Udp.h"
#include "Dns.h"
#include "Arduino.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "aes256.h"

RPCClient::RPCClient(EthernetUDP E_udp) {
	_E_udp = E_udp; 
	_method_id = 0;
	_order = 1;	
	_encript = false;
}

//데이터전송할때
//int RPCClient::excute(const char *host, uint16_t port, char *method_name, char *extension, 
//	char *language, int order, int bufSize, int fileSize)
//{
//	//멤버변수 초기화
//	_bufSize = bufSize;
//	int hostlen = strlen(host);
//	strncpy(_host,host,hostlen);
//	_port = port;
//
//	//프로퍼티 생성
//
//	char properties[100] = "start,";
//	char comma[2] = ",";
//
//	char u_method_id[100];
//	itoa(_method_id,u_method_id,10);
//	strncat( properties, u_method_id, strlen(u_method_id)); 
//	_method_id++;
//	strncat( properties, comma, strlen(comma));
//	strcat( properties, method_name);
//	strncat( properties, comma, strlen(comma));
//	strcat( properties, extension);
//	strncat( properties, comma, strlen(comma));
//	strcat( properties, language);
//	strncat( properties, comma, strlen(comma));
//	char u_order[100];
//	itoa(order, u_order, 10);
//	strncat( properties, u_order, strlen(u_order));
//	strncat( properties, comma, strlen(comma));
//	char u_fileSize[100];
//	itoa(fileSize, u_fileSize, 10);
//	strncat( properties, u_fileSize, strlen(u_fileSize));
//
//	_E_udp.beginPacket(host,port);
//	_E_udp.write(properties);
//	_E_udp.endPacket();
//
//
//	while(true)//property_ack - while
//	{
//		int packetSize = _E_udp.parsePacket();
//
//		if(packetSize)
//		{
//			char propertyAck[UDP_TX_PACKET_MAX_SIZE];
//			_E_udp.read(propertyAck,UDP_TX_PACKET_MAX_SIZE);
//
//			if(strncmp(propertyAck,"ack",3)==0)
//			{
//				//데이터 전송
//				char data[DATA_MAX_SIZE] ="";
//				strcpy(data,databuffer);
//				int data_len = strlen(data);
//
//				_E_udp.beginPacket(host,port);
//				_E_udp.write(data);
//				_E_udp.endPacket();
//
//				while(true)//data_ack - while
//				{
//					int packetSize = _E_udp.parsePacket();
//
//					if(packetSize)
//					{
//						char dataAck[UDP_TX_PACKET_MAX_SIZE];
//						_E_udp.read(dataAck,UDP_TX_PACKET_MAX_SIZE);
//
//						if(strncmp(dataAck,"ack",3)==0)
//						{
//							//end 전송
//							char end[4] = "end";
//
//							_E_udp.beginPacket(host,port);
//							_E_udp.write(end);
//							_E_udp.endPacket();
//
//							while(true)//end_ack - while
//							{
//								int packetSize = _E_udp.parsePacket();
//
//								if(packetSize)
//								{
//									char endAck[UDP_TX_PACKET_MAX_SIZE];
//									_E_udp.read(endAck,UDP_TX_PACKET_MAX_SIZE);
//
//									if(strncmp(endAck,"ack",3)==0)
//									{
//										//exe 전송
//										char exe[4] = "exe";
//
//										_E_udp.beginPacket(host,port);
//										_E_udp.write(exe);
//										_E_udp.endPacket();
//
//										while(true)
//										{
//											int packetSize = _E_udp.parsePacket();
//
//											if(packetSize)
//											{
//												_E_udp.read(_returnBuffer,UDP_TX_PACKET_MAX_SIZE);
//												return _returnBuffer;
//											}
//										}
//									}else
//									{
//										//debugging or nothing to do
//										//return 2;//end 전송 에러
//									}
//								}
//							}
//						}else
//						{
//							//debugging or nothing to do
//							//return 1;//데이터 전송 에러
//						}
//					}
//				}
//			}else
//			{
//				//debugging or nothing to do
//				//return 0;//프로퍼티 전송 에러
//			}
//		}
//	}
//}

//int RPCClient::excute(IPAddress ip, uint16_t port, char *method_name, char *extension, 
//	char *language, int order, int bufSize, int fileSize)
//{
//
//	_bufSize = bufSize;
//	_ip = ip;
//	_port = port;
//	//프로퍼티 생성
//
//	char properties[100] = "start,";
//	char comma[2] = ",";
//
//	char u_method_id[100];
//	itoa(_method_id,u_method_id,10);
//	strncat( properties, u_method_id, strlen(u_method_id)); 
//	_method_id++;
//	strncat( properties, comma, strlen(comma));
//	strcat( properties, method_name);
//	strncat( properties, comma, strlen(comma));
//	strcat( properties, extension);
//	strncat( properties, comma, strlen(comma));
//	strcat( properties, language);
//	strncat( properties, comma, strlen(comma));
//	char u_order[100];
//	itoa(order, u_order, 10);
//	strncat( properties, u_order, strlen(u_order));
//	strncat( properties, comma, strlen(comma));
//	char u_bufSize[100];
//	itoa(_bufSize, u_bufSize, 10);
//	strncat( properties, u_bufSize, strlen(u_bufSize));
//	strncat( properties, comma, strlen(comma));
//	char u_fileSize[100];
//	itoa(fileSize, u_fileSize, 10);
//	strncat( properties, u_fileSize, strlen(u_fileSize));
//
//	_E_udp.beginPacket(_ip,_port);
//	_E_udp.write(properties);
//	_E_udp.endPacket();
//
//	return 1;
//	//while(true)//property_ack - while
//	//{
//	/*	int packetSize = _E_udp.parsePacket();
//
//		if(packetSize)
//		{
//			char propertyAck[UDP_TX_PACKET_MAX_SIZE];
//			_E_udp.read(propertyAck,UDP_TX_PACKET_MAX_SIZE);
//
//			if(strncmp(propertyAck,"ack",3)==0)
//			{
//				return 1;
//			}
//			else
//				return 0;
//		}*/
//	//}
//	
//}
//int RPCClient::excute(const char *host, uint16_t port, char *method_name, char *extension, 
//	char *language, int order, int bufSize, int fileSize)
//{
//
//	//멤버변수 초기화
//	//_bufSize = bufSize;
//	//int hostlen = strlen(host);
//	//strncpy(_host,host,hostlen);
//	//_port = port;
//
//	//프로퍼티 생성
//
//	char properties[100] = "start,";
//	char comma[2] = ",";
//
//	char u_method_id[100];
//	itoa(_method_id,u_method_id,10);
//	strncat( properties, u_method_id, strlen(u_method_id)); 
//	_method_id++;
//	strncat( properties, comma, strlen(comma));
//	strncat( properties, method_name, strlen(method_name));
//	strncat( properties, comma, strlen(comma));
//	strncat( properties, extension, strlen(extension));
//	strncat( properties, comma, strlen(comma));
//	strncat( properties, language, strlen(language));
//	strncat( properties, comma, strlen(comma));
//	char u_order[100];
//	itoa(order, u_order, 10);
//	strncat( properties, u_order, strlen(u_order));
//	strncat( properties, comma, strlen(comma));
//	char u_bufSize[100];
//	itoa(_bufSize, u_bufSize, 10);
//	strncat( properties, u_bufSize, strlen(u_bufSize));
//	strncat( properties, comma, strlen(comma));
//	char u_fileSize[100];
//	itoa(fileSize, u_fileSize, 10);
//	strncat( properties, u_fileSize, strlen(u_fileSize));
//
//	_E_udp.beginPacket(host,port);
//	_E_udp.write(properties);
//	_E_udp.endPacket();
//
//	return 1;
//	//while(true)//property_ack - while
//	//{
//	/*	int packetSize = _E_udp.parsePacket();
//
//		if(packetSize)
//		{
//			char propertyAck[UDP_TX_PACKET_MAX_SIZE];
//			_E_udp.read(propertyAck,UDP_TX_PACKET_MAX_SIZE);
//
//			if(strncmp(propertyAck,"ack",3)==0)
//			{
//				return 1;
//			}else
//				return 0;
//		}*/
//	//}
//}



int RPCClient::execute(const char *host, uint16_t port, char *method_name, char *extension, 
	char *language, int order, int fileSize)
{

	strncpy(_host,host,strlen(host));
	_port = port;
	char properties[100] = "start,";
	char comma[2] = ",";

	char u_method_id[10];
	itoa(_method_id,u_method_id,10);
	strncat( properties, u_method_id, strlen(u_method_id)); 
	_method_id++;
	strncat( properties, comma, strlen(comma));
	strncat( properties, method_name, strlen(method_name));
	strncat( properties, comma, strlen(comma));
	strcat( properties, extension);
	strncat( properties, comma, strlen(comma));
	strcat( properties, language);
	strncat( properties, comma, strlen(comma));
	char u_order[10];
	itoa(order, u_order, 10);
	strncat( properties, u_order, strlen(u_order));
	strncat( properties, comma, strlen(comma));
	char u_fileSize[10];
	itoa(fileSize, u_fileSize, 10);
	strncat( properties, u_fileSize, strlen(u_fileSize));

	_E_udp.beginPacket(_host,_port);
	_E_udp.write(properties);
	_E_udp.endPacket();

	while(true)
	{
		int packetSize = _E_udp.parsePacket();

		if(packetSize)
		{
			char propertyAck[UDP_TX_PACKET_MAX_SIZE];
			_E_udp.read(propertyAck,UDP_TX_PACKET_MAX_SIZE);

			if(strncmp(propertyAck,"ack1",4)==0)
			{
				return 1;
			}
		}
	}
}

int RPCClient::execute(IPAddress ip, uint16_t port, char *method_name, char *extension, 
	char *language, int order, int fileSize)
{

	_ip = ip;
	_port = port;
	char properties[100] = "start,";
	char comma[2] = ",";

	/*char u_method_id[10];
	itoa(_method_id,u_method_id,10);
	strncat( properties, u_method_id, strlen(u_method_id)); 
	_method_id++;*/
	strncat( properties, "abc", strlen("abc"));
	strncat( properties, comma, strlen(comma));
	strncat( properties, method_name, strlen(method_name));
	strncat( properties, comma, strlen(comma));
	strcat( properties, extension);
	strncat( properties, comma, strlen(comma));
	strcat( properties, language);
	strncat( properties, comma, strlen(comma));
	char u_order[10];
	itoa(order, u_order, 10);
	strncat( properties, u_order, strlen(u_order));
	strncat( properties, comma, strlen(comma));
	char u_fileSize[10];
	itoa(fileSize, u_fileSize, 10);
	strncat( properties, u_fileSize, strlen(u_fileSize));

	_E_udp.beginPacket(_ip,_port);
	_E_udp.write(properties);
	_E_udp.endPacket();

	while(true)
	{
		int packetSize = _E_udp.parsePacket();

		if(packetSize)
		{
			char propertyAck[UDP_TX_PACKET_MAX_SIZE];
			_E_udp.read(propertyAck,UDP_TX_PACKET_MAX_SIZE);

			if(strncmp(propertyAck,"ack1",4)==0)
			{
				return 1;
			}
		}
	}
}

void RPCClient::setEncryptKey(uint8_t *key)
{
	//key값 설정]
	uint8_t i;
	for (i = 0; i < sizeof(key);i++) _key[i] = key[i];
	_encript = true;
}

void RPCClient::sendParam(uint8_t *readFile, int size)
{
	if(_encript)//encript true
	{
		//AES256 encrypt
		aes256_context ctx; 
		uint8_t buf[16], i;

		//buf복사
		for (i = 0; i < sizeof(buf);i++) buf[i] = readFile[i];

		//encryption
		aes256_init(&ctx, _key);
		aes256_encrypt_ecb(&ctx, buf);

		//data transmit
		if(_host != 0)
			_E_udp.beginPacket(_host,_port);
		else if(_ip != 0)
			_E_udp.beginPacket(_ip,_port);

		_E_udp.write(buf,size);
		_E_udp.endPacket();

		aes256_done(&ctx);

	}else//encript false
	{
		//data transmit
		if(_host != 0)
			_E_udp.beginPacket(_host,_port);
		else if(_ip != 0)
			_E_udp.beginPacket(_ip,_port);

		_E_udp.write(readFile,size);
		_E_udp.endPacket();
	}
}

void RPCClient::endParam()
{
	//end 전송
	char end[4] = "end";
	if(_host != 0)
		_E_udp.beginPacket(_host,_port);
	else if(_ip != 0)
		_E_udp.beginPacket(_ip,_port);
	_E_udp.write(end);
	_E_udp.endPacket();
}

int RPCClient::waitReturnValue()
{

	//end 전송
	char end[4] = "end";

	if(_host != 0)
		_E_udp.beginPacket(_host,_port);
	else if(_ip != 0)
		_E_udp.beginPacket(_ip,_port);
	_E_udp.write(end);
	_E_udp.endPacket();

	while(true)
	{
		int packetSize = _E_udp.parsePacket();

		if(packetSize)
		{
			char EndAck[UDP_TX_PACKET_MAX_SIZE];
			_E_udp.read(EndAck,UDP_TX_PACKET_MAX_SIZE);

			if(strncmp(EndAck,"ack2",4)==0)
			{
				//exe 전송
				char exe[20] = "exe_abc";
				/*char u_method_id[10];
				itoa(_method_id,u_method_id,10);
				strncat(exe,u_method_id,strlen(u_method_id));*/

				if(_host != 0)
					_E_udp.beginPacket(_host,_port);
				else if(_ip != 0)
					_E_udp.beginPacket(_ip,_port);
				_E_udp.write(exe);
				_E_udp.endPacket();

				while(true)
				{
					int packetSize = _E_udp.parsePacket();

					if(packetSize)
					{
						char returnValue[UDP_TX_PACKET_MAX_SIZE];
						_E_udp.read(returnValue,UDP_TX_PACKET_MAX_SIZE);
						int rvalue = atoi(returnValue);
						//return (void*)rvalue;
						return rvalue;
					}
				}
			}
		}
	}
}

uint8_t RPCClient::begin(uint16_t port) {
	return _E_udp.begin(port);
}

int RPCClient::available() {
	return _E_udp.remaining();
}

void RPCClient::stop()
{
	_E_udp.stop();
}

int RPCClient::beginPacket(const char *host, uint16_t port)
{
	_E_udp.beginPacket(host,port);
}

int RPCClient::beginPacket(IPAddress ip, uint16_t port)
{
	return _E_udp.beginPacket(ip,port);
}

int RPCClient::endPacket()
{
	return _E_udp.endPacket();
}

size_t RPCClient::write(uint8_t byte)
{
	return write(&byte, 1);
}

size_t RPCClient::write(const uint8_t *buffer, size_t size)
{
	return _E_udp.write(buffer,size);
}

int RPCClient::parsePacket()
{
	_E_udp.parsePacket();
}

int RPCClient::read()
{
	_E_udp.read();
}

int RPCClient::read(unsigned char* buffer, size_t len)
{

	return _E_udp.read(buffer,len);

}
int RPCClient::peek()
{
	return _E_udp.peek();
}
void RPCClient::flush()
{
	_E_udp.flush();
}
