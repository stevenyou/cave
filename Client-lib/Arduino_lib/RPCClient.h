#ifndef RPCClient_h
#define RPCClinet_h


#include "EthernetUdp.h"
#include <Udp.h>
#include <string.h>

#define UDP_TX_PACKET_MAX_SIZE 24
#define DATA_MAX_SIZE          375
//typedef unsigned char uint8_t;
//typedef unsigned short uint16_t;

class RPCClient : public UDP{

private:
  
  EthernetUDP _E_udp;
 
  char _host[16];
  
  IPAddress _ip;
  
  uint16_t _port;
 
  bool _encript;
  
  int _method_id; //함수 콜 고유번호 (중복X)
 
  int _order; //순서보장1, 아니면 0
  
  int _fileSize; //파일크기
  
  uint8_t _key[32];
 
  char _returnBuffer[UDP_TX_PACKET_MAX_SIZE];

public:
  
  RPCClient(EthernetUDP E_udp);  // Constructor

  int execute(const char *host, uint16_t port, char *method_name, char *extension, char *language, int order, int fileSize);

  int execute(IPAddress ip, uint16_t port, char *method_name, char *extension, char *language, int order, int fileSize);

  void setEncryptKey(uint8_t *key);

  void sendParam(uint8_t *readFile, int size);

  void endParam();

  int waitReturnValue();
  
  uint8_t begin(uint16_t);	

  void stop();  

  int beginPacket(IPAddress ip, uint16_t port);

  int beginPacket(const char *host, uint16_t port);

  int endPacket();

  size_t write(uint8_t);

  size_t write(const uint8_t *buffer, size_t size);

  using Print::write;

  int parsePacket();

  int available();

  int read();

  int read(unsigned char* buffer, size_t len);

  int read(char* buffer, size_t len) { return read((unsigned char*)buffer, len); };

  int peek();

  void flush();	

  IPAddress remoteIP() { return _E_udp.remoteIP(); };

  uint16_t remotePort() { return _E_udp.remotePort(); };

};

#endif