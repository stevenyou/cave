package com.ssm.rpc.caves.concerrent;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.io.DataInputStream;

/*
 *  Mapping By Device
 *  디바이스 1개에 mapping
 */
public class ConcerrentWorker implements Runnable {
	private volatile Object key;
	private Socket sock;
	private String id;
	private volatile DataOutputStream writer;
	private volatile DataInputStream reader;
	private String message;
	private boolean end;
	// Handle the ProcessInvoker
	// all of thing is discriminarse with Client Input(some flag)
	// Using Vector to avoid synchronization Issue
	private LinkedHashMap<String, Future<LinkedList<String>>> taskQ;
	private Vector<Future<LinkedList<String>>> taskContainer;
	// using process (thread to generate Process)
	private ExecutorService innerExeService;
	private boolean readyEnd = false;
	// to Distinguish the Client(Device)
	// key = function nama : Value = Invoker
	private volatile Map<String, ProcessInvoker> processMap;
	private volatile Map<String, MessageAnalyzer> analyzerMap;
	// divide multimedia & literal type if params
	private int isMulti;
	// only multimedia data
	private int allLength;
	private int read_file;

	// Constructor
	public ConcerrentWorker(Socket sock, int funtionPoolSize) {
		System.out.println("ConcerrentWorker_Constructor");
		this.key = new Object();
		this.id = "*";
		this.end = true;
		this.sock = sock;
		this.message = null;
		this.isMulti = 0;
		this.allLength = 0;
		this.read_file = 0;
		this.innerExeService = Executors.newFixedThreadPool(funtionPoolSize);
		// Synchronization DataStructure
		this.analyzerMap = new LinkedHashMap<String, MessageAnalyzer>();
		this.processMap = new LinkedHashMap<String, ProcessInvoker>();
		this.taskQ = new LinkedHashMap<String, Future<LinkedList<String>>>();
		try {
			this.writer = new DataOutputStream(sock.getOutputStream());
			this.reader = new DataInputStream(sock.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // Constructor End

	public void run() {
		while (true) {
			String line = null;
			try {
				if(read_file != 1 && isMulti != 1){
					line = reader.readUTF();
				}
			} catch (IOException e) {
//				e.printStackTrace();
			}
			// System.out.println("line : " + line);
			if (line != null || (read_file == 0 && isMulti == 1)) {
				flow(line);
			} else if (read_file == 1 && isMulti == 1) {
				getMultiParamFile();
			}
		}
	}

	public void getMultiParamFile() {
		isMulti = 0;
		read_file = 0;
		System.out.println("ConcerrentWorker_run_실제처리_Multimedia");
		// Multimedia Type
		// 기존에 미리 fix
		int length = allLength;

		System.out.println("allLength : " + allLength);

		byte[] allBuff = new byte[length];
		try {
			reader.readFully(allBuff);
			
			// To keep the Tcp Socket Connection
			writer.writeUTF("ack1");
			
			// allBuff[0] : string으로 읽을 바이트 배열의 크기
			int len = allBuff[0];
			System.out.println("len : " + len);
			byte propBuff[] = new byte[len - 1];
			for (int i = 1, j = 0; i < len; i++, j++) {
				propBuff[j] = allBuff[i];
			}
			String line = new String(propBuff, "UTF-8");
			if (line.equals("$")) {
				// END
				readyEnd = true;
				checkTask();
				sock.close();
				end = false;
			}
			// LINE : [1] ~ [N]
			// LINE : methodID(파일이름), lang, howMany, securityOrder,
			// extension < OR > $(socket close)
			// MultiMedia Socket
			// get real params direct
			// methodID(파일이름), howMany, securityOrder, extension
			
			
			String methodId = line.split(",")[0];
			line = line.replace(methodId + ",", "");
			analyzerMap.get(methodId).getMultiParams(line, allBuff, len);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void flow(String line) {
		String message = line;
		System.out.println("ConcerrentWorker_run_While");
		// 첫 요청 (Property)
		// << First protocol >>
		// id(deviceId) / ismulti(0,1)
		// --------> (multimedia type의 socket의 경우)
		// allenth(전체 읽기) / method ID
		// --------> (Literal type의 socket의 경우)
		// none
		if (this.id.equals("*")) {
			System.out.println("ConcerrentWorker_run_First");
			if (message != null) {
				// 탄이와 protocol을 맞춰야 된다.
				id = message.split(",")[0];
				isMulti = Integer.parseInt(message.split(",")[1]);
				// Ack to Client
				// Clinet pass on the Parameter's
				// When they delivered the Ack(1)
				if (isMulti == 1) {
					read_file = 1;
					System.out.println("ConcerrentWorker_run_First_IsMulti");
					message = message.replace(id + "," + isMulti + ",", "");
					// 첫번째 allLength -----> 이후에 올 property (methodId, 파일의 이름 ,
					// extension , 실제 byteStream)의 총합
					// 이후의 protocol ----->
					// string으로 읽을 바이트 배열의 크기 (첫번째칸), methodID(파일이름), lang,
					// howMany, securityOrder, extension, 실제 byteStream
					allLength = Integer.parseInt(message.split(",")[0]);
					String methodId = message.split(",")[1];
					MessageAnalyzer msgAnalyzer = new MultiMsgAnalzer(
							processMap, taskQ, innerExeService,
							key, writer, reader, allLength, methodId);
					msgAnalyzer.setId(methodId);
					analyzerMap.put(methodId, msgAnalyzer);
				}
				try {
					// Ack to client(Device)
					writer.writeUTF("1");
					writer.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				Thread.yield();
			}
		} else if (!readyEnd) {
			// 실제 처리부
			System.out.println("ConcerrentWorker_run_실제처리");

			System.out.println("ConcerrentWorker_run_실제처리_literal Type");
			// literal Type
			try {
				// Any function shut down
				if (line.equals("$")) {
					System.out.println("ConcerrentWorker_run_실제처리_END_$");
					// END
					readyEnd = true;
					checkTask();
					sock.close();
					end = false;
				} else {
					if (line.split(",")[0].equals("prop")) {
						System.out
								.println("ConcerrentWorker_run_실제처리__literal_prop");
						// GET the Parameter Data's Property
						line = line.replace("prop,", "");
						// the method Id
						// each method can has a multiple Id(methodId)
						String methodId = line.split(",")[0];
						line = line.replace(methodId + ",", "");
						// get Parameter Property & generate Procedure
						// and Real Parameter

						System.out.println("methodId : " + methodId);

						if (analyzerMap.containsKey(methodId)) {
							System.out.println("Already_ConTain_get_Property");
							analyzerMap.get(methodId).getParamsProperty(line);
						} else {
							synchronized (methodId) {
								MessageAnalyzer msgAnalyzer = null;
								msgAnalyzer = new LiteralMsgAnalzer(processMap,
										taskQ, innerExeService,
										key, writer, reader, methodId);
								msgAnalyzer.setId(methodId);
								analyzerMap.put(methodId, msgAnalyzer);
								msgAnalyzer.getParamsProperty(line);
							}
						}
					} else {
						System.out
								.println("ConcerrentWorker_run_실제처리__literal_RealData");
						// GET the real Parameter Data
						String methodId = line.split(",")[0];
						line = line.replace(methodId + ",", "");
						if (analyzerMap.containsKey(methodId)) {
							System.out
									.println("Already_ConTain_getParamsProperty");
							analyzerMap.get(methodId).getRealParms(line);
						} else {
							writer.writeUTF("-1");
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				// while (true) {
				// for (int i = 0; i < taskContainer.size(); i++) {
				// if (taskContainer.get(i).isDone()) {
				// taskContainer.remove(i);
				// } else {
				// Thread.yield();
				// }
				// }
				// if (taskContainer.size() == 0 && taskQ.size() == 0) {
				// try {
				// if (sock != null)
				// sock.close();
				// } catch (Exception ex) {
				// }
				// break;
				// }
				// }
			}
		}
	}

	public void checkTask() {
		while (true) {
			for (int i = 0; i < taskContainer.size(); i++) {
				if (taskContainer.get(i).isDone()) {
					taskContainer.remove(i);
				} else {
					Thread.yield();
				}
			}
			if (taskContainer.size() == 0 && taskQ.size() == 0) {
				break;
			}
		}
	}

}
