package com.ssm.rpc.caves.concerrent;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.swing.text.html.HTMLDocument.Iterator;

import com.ssm.rpc.caves.process.RpcProcessGen;
import com.ssm.rpc.caves.process.RpcProcessJ;

// Mapping By Request (in Device)
// To generate the Process, which is requested by specific Device
// It handed by the ConcerrentWorker (task queue)
public class ProcessInvoker implements Callable<LinkedList<String>> {

	// to synchronize
	private String method;
	private Object key;
	private String lang;
	private DataOutputStream writer;
	// to file
	private int isArray;
	private List<String> params;
	private volatile LinkedHashMap<String, Future<LinkedList<String>>> taskQ;
	private volatile Vector<Future<LinkedList<String>>> taskContainer;
	// to file
	private String orderGuarantee;
	private String MethodId;
	private String paramfile = "*";

	// constructor

	public ProcessInvoker(DataOutputStream writer, Object key, String lang,
			String methodId, String method, String paramfile,
			String orderGuarantee) {
		System.out.println("ProcessInvoker_Constructor");
		this.method = method;
		this.MethodId = methodId;
		this.writer = writer;
		this.key = key;
		this.lang = lang;
		this.orderGuarantee = orderGuarantee;
		if (paramfile != null) {
			this.paramfile = paramfile;
		}
	}

	// methods
	public LinkedHashMap<String, Future<LinkedList<String>>> getTaskQ() {
		return taskQ;
	}

	public ProcessInvoker setTaskQ(
			LinkedHashMap<String, Future<LinkedList<String>>> taskQ) {
		this.taskQ = taskQ;
		return this;
	}

	public Vector<Future<LinkedList<String>>> getTaskContainer() {
		return taskContainer;
	}

	public ProcessInvoker setTaskContainer(
			Vector<Future<LinkedList<String>>> taskContainer) {
		this.taskContainer = taskContainer;
		return this;
	}

	public List<String> getParam() {
		return params;
	}

	public ProcessInvoker setParam(List<String> params) {
		this.params = params;
		return this;
	}

	public int isArray() {
		return isArray;
	}

	public ProcessInvoker setArray(int isArray) {
		this.isArray = isArray;
		return this;
	}

	public String getMethodId() {
		return MethodId;
	}

	public void setMethodId(String methodId) {
		MethodId = methodId;
	}

	public String getParamfile() {
		return paramfile;
	}

	public void setParamfile(String paramfile) {
		this.paramfile = paramfile;
	}

	@Override
	public LinkedList<String> call() throws Exception {
		if (lang.equalsIgnoreCase("java")) {
			// For test
			String dir = "C:\\lecture\\testcmd\\src\\test";
			LinkedList<String> answer = new RpcProcessJ(dir, method, paramfile,
					params, new LinkedList<String>()).excute();
			System.out.println("Answer : " + answer);
			returnAnswer(answer);
			return answer;
		} else if (lang.equalsIgnoreCase("c")) {
			/*
			 * 진기....
			 */
		}
		return null;
	}

	public void returnAnswer(LinkedList<String> answer) {
		System.out.println("returnAnswer");
		if (orderGuarantee.equals("0")) {
			System.out.println("orderGuarantee = 보장안함");
			// 보장 x --> 끝난 순서대로 전송
			// if this process is the first request from Device(Client)

			for (int i = 0; i < answer.size(); i++) {
				try {
					taskQ.remove(MethodId);
					writer.writeUTF(answer.get(i));
					writer.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("orderGuarantee = 보장");
			// 보장 0 --> 요청된 순서대로 전송
			while (true) {
				java.util.Iterator<String> itr = taskQ.keySet().iterator();
				String firstTask = itr.next();
				if (this.MethodId.equals(firstTask)) {
					// if this process is the first request from Device(Client)
					taskQ.remove(firstTask);

					if (answer.get(0).equalsIgnoreCase("isArray")) {
						// Array to client(Device)
						try {
							writer.writeUTF(answer.get(0)); // isArray
							writer.flush();
							String cnt = answer.get(1);
							int cnt_i = Integer.parseInt(cnt);
							writer.writeUTF(answer.get(1)); // array Count(어레이 개수)
							writer.flush();
							int k = 2;
							for(int i = 0; i < cnt_i; i++){
								String array_size = answer.get(k);
								int array_size_i = Integer.parseInt(array_size) + k;
								k++;
								for(int j = k; j < array_size_i; j++){
									writer.writeUTF(answer.get(j)); // array Count(어레이 개수)
									writer.flush();
								}
								writer.writeUTF("$"); // array Count(어레이 개수)
								writer.flush();
							}
						} catch (IOException e1) {
							e1.printStackTrace();
						} 
					} else {
						// literal to client(Device)
						for (int i = 0; i < answer.size(); i++) {
							try {
								writer.writeUTF(answer.get(i));
								writer.flush();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						break;
					}
				} else {
					notifyAll();
				}
			}
		}
	}

}
