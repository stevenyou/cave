package com.ssm.rpc.caves.concerrent;

import java.beans.FeatureDescriptor;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.ssm.rpc.caves.contibution.RpcInit;

public class MultiMsgAnalzer extends MessageAnalyzer {

	private int allLength;
	private int totalSize;
	private String extension;
	
	public MultiMsgAnalzer(Map<String, ProcessInvoker> processMap,
			LinkedHashMap<String, Future<LinkedList<String>>> taskQ,
			ExecutorService innerExeService, Object key,
			DataOutputStream writer, DataInputStream reader, int allLength, String MethodId) {
		super(processMap, taskQ, innerExeService, writer,
				reader, MethodId, innerExeService);
		
		System.out.println("MultiMsgAnalzer_CONSTRUCTOR");
		
		this.allLength = allLength;
		this.totalSize = 0;
		this.extension = null;
	}

	public void getParamsProperty(String line) {}
	public void getRealParms(String line) {}

	@Override
	public void getMultiParams(String line, byte[] allbuf, int len) {
		// Generate File(using by parameter)
		// methodName, lang, howMany, securityOrder, extension
		
		String[] props = line.split(",");
		String methodName = props[0];
		String lang = props[1];
		String securityOrder = props[2];
		String extension = props[3];
		int end = Integer.parseInt(props[4]);
		int totalSize = allLength - len;
		
		System.out.println("methodName : " + methodName);
		System.out.println("lang : " + lang);
		System.out.println("securityOrder : " + securityOrder);
		System.out.println("extension : " + extension);
		System.out.println("end : " + end);
		System.out.println("totalSize : " + totalSize);
		
		try ( FileOutputStream out = new FileOutputStream(RpcInit.prop.get(System.getProperty("os.name").split(" ")[0])
													      .get("paramFileDir") +
														  System.getProperty("file.separator") +	
														  this.MethodId + "." + extension)) {
			int k = len; 
			int i = 0;
			byte[] buf = new byte[totalSize]; 
			while (i < totalSize) {
				buf[i] = allbuf[k];
				i++; k++;
			}
			out.write(buf); // 파일에 write
		} catch (Exception e) {
			e.printStackTrace();
		} 
		

		try {
			// To keep the Tcp Socket Connection
			writer.writeUTF("ack2");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if (end == 1) {
			ProcessInvoker process = new ProcessInvoker(writer, this.key, lang, MethodId, 
													    methodName, (MethodId+"."+extension), securityOrder);
			synchronized (this.key) {
				processMap.put(MethodId, process);
				processMap.get(MethodId).setTaskQ(taskQ).setParam(params);
				taskQ.put(MethodId, innerExeService.submit(processMap.get(MethodId)));
			}
		}
	}

}
