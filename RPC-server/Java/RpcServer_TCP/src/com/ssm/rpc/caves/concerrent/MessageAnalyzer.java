package com.ssm.rpc.caves.concerrent;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.ssm.rpc.caves.contibution.RpcManager;

public abstract class MessageAnalyzer {
	
	
	protected Map<String, ProcessInvoker> processMap;
	protected volatile LinkedHashMap<String, Future<LinkedList<String>>> taskQ;
	protected ExecutorService innerExeService;
	protected Object key;
	protected volatile DataOutputStream writer;
	protected volatile  DataInputStream reader;
	protected volatile String MethodId;
	protected String securityOrder;
	protected List<String> params;

	
	
	// constructor
	public MessageAnalyzer(Map<String, ProcessInvoker> processMap ,
						   LinkedHashMap<String, Future<LinkedList<String>>> taskQ ,
						   Object key, DataOutputStream writer ,
						   DataInputStream reader,
						   String MethodId, ExecutorService innerExeService){
		this.MethodId = MethodId;
		this.processMap = processMap;
		this.taskQ = taskQ;
		this.innerExeService = innerExeService;
		this.key = key;
		this.writer = writer;
		this.reader = reader;
		this.securityOrder = "";
		this.params = new ArrayList<String>();

	}
	
	//get, set
	public Map<String, ProcessInvoker> getProcessMap() {
		return processMap;
	}

	public void setProcessMap(Map<String, ProcessInvoker> processMap) {
		this.processMap = processMap;
	}

	public LinkedHashMap<String, Future<LinkedList<String>>> getTaskQ() {
		return taskQ;
	}

	public synchronized String getId() {
		return MethodId;
	}

	public synchronized void setId(String MethodId) {
		this.MethodId = MethodId;
	}

	public void setTaskQ(LinkedHashMap<String, Future<LinkedList<String>>> taskQ) {
		this.taskQ = taskQ;
	}
	
	public abstract void getParamsProperty(String line);
	public abstract void getRealParms(String line);
	public abstract void getMultiParams(String line, byte[] allbuf, int len);
}
