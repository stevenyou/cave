package com.ssm.rpc.caves.process;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class RpcProcessJ implements RpcProcessGen{
	private LinkedList<String> answer;
	private String dir;
	private String toGenerate;
	private String paramfile = "*";
	private List<String> params;
	
	public RpcProcessJ (String dir, String gen, String paramfile, 
					 	List<String> params, LinkedList<String> answer){
		this.dir = dir.replace("/", filespr);
		this.dir = dir.replace("\\", System.getProperty("file.separator"));
		this.toGenerate = gen;
		this.paramfile = paramfile;
		this.params = params;
		this.answer = answer;
	}
	
	public LinkedList<String> excute(){
		System.out.println("RpcProcessJ_excute()");
		try {
			int at = dir.lastIndexOf(filespr);
			String classpath = dir.substring(at+1);
			String cmd[] = new String[3];

			cmd[0] = "cmd.exe";
			cmd[1] = "/C";
			if(paramfile.equals("*")){
				cmd[2] = "cd " + dir + " && " +
						 "java " + classpath + "." + toGenerate;
				if(params.size() > 0){
					for(int i = 0 ; i < params.size(); i++){
						cmd[2] += " " + params.get(i);
					}
				}
			} else {
				cmd[2] = "cd " + dir + " && " +
						 "java " + classpath + "." + toGenerate + " " + paramfile;
				if(params.size() > 0){
					for(int i = 0 ; i < params.size(); i++){
						cmd[2] += " " + params.get(i);
					}
				}
			}
			Runtime runTime = Runtime.getRuntime();
			Process process = null;
			try {
				process = runTime.exec(cmd);
				processCatcher gb1 = new processCatcher(process.getInputStream(), answer);
				processCatcher gb2 = new processCatcher(process.getErrorStream());
				gb1.start();
				gb2.start();
				while (true) {
					if (!gb1.isAlive() && !gb2.isAlive()) { 
						System.out.println("Thread gb1 Status : "
								+ gb1.getState());
						System.out.println("Thread gb2 Status : "
								+ gb1.getState());
						process.waitFor();
						break;
					}
				}
			} catch (Exception e) {

			} finally {
				if (process != null)
					process.destroy();
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		
		// test 
		if(answer.size() != 0){
			for (int i = 0; i < answer.size(); i++) {
				System.out.println("Answer : " + answer.get(i));
			}
		}
		
		return answer;
	}
}
