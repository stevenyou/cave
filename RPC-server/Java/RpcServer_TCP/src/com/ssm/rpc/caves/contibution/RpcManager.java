package com.ssm.rpc.caves.contibution;
import java.util.HashMap;

/**
 * RpcManager
 * 
 * single tone
 * rpc data 전체를 관장
 * 
 */
public class RpcManager {
	
	private final static String os = System.getProperty("os.name").split(" ")[0];
	private static volatile RpcManager instance = null;
	private RpcInit rpcit;
	private RpcSkeletone rpcSk;
	// to single tone
	private RpcManager(String mod){
		// 저절로 single_tone
		rpcit = new RpcInit(mod);
		rpcSk = new RpcSkeletone(
				Integer.parseInt(rpcit.getProp().get(os).get("poolsize")),
				Integer.parseInt(rpcit.getProp().get(os).get("rpcport")),
				Integer.parseInt(rpcit.getProp().get(os).get("functionpool")));
		}
	
	// get instance protected by race condition
	public static RpcManager getInstance(){
		if(instance == null){
			synchronized (RpcManager.class){
				if(instance == null){
					instance = new RpcManager(os);
				}
			}
		}
		return instance;
	}
	
	
	//test
	public static void main(String[] args){
		RpcManager.getInstance();
	}
	
	
}
