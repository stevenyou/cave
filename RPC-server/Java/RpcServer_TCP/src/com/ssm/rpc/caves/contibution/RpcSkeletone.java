package com.ssm.rpc.caves.contibution;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.ssm.rpc.caves.concerrent.ProcessInvoker;
import com.ssm.rpc.caves.concerrent.ConcerrentWorker;


public class RpcSkeletone {
	// Device(Client)처리 개수
	// 같은 Device 에서 온 task들은 (return 전에 또 온 것들)
	// 내부의Task queue로 관리
	private ExecutorService exeService;
	private Object key;
	private Set<String> devicelist;
	private int funtionPoolSize = 1;
	
	public RpcSkeletone(int poolSize, int portNum, int funtionPoolSize){
		key = new Object();
		exeService = Executors.newFixedThreadPool(poolSize);
		this.funtionPoolSize = funtionPoolSize;
		runServ(portNum);
	}
	
	public void runServ(int portNum){
		try {
			// portNum은 추후 변환
			ServerSocket server = new ServerSocket(portNum);
			System.out.println("TCP/IP : Wait for Device request.");
			while (true) {
				System.out.println("while");
				synchronized (key) {
					Socket sock = server.accept();
					ConcerrentWorker worker = new ConcerrentWorker(sock, funtionPoolSize); 
 					exeService.execute(worker);
				}
			} // while
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
