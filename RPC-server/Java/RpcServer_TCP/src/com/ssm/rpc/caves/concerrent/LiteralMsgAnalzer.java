package com.ssm.rpc.caves.concerrent;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import com.ssm.rpc.caves.contibution.RpcInit;

public class LiteralMsgAnalzer extends MessageAnalyzer {
	
	private int isArray;
	private int count;
	private int size;
	private int end;
	
	public LiteralMsgAnalzer(Map<String, ProcessInvoker> processMap,
			LinkedHashMap<String, Future<LinkedList<String>>> taskQ,
			ExecutorService innerExeService, Object key,
			DataOutputStream writer, DataInputStream reader, String MethodId) {
		super(processMap, taskQ, innerExeService, writer,
				reader, MethodId, innerExeService);
		this.isArray = 0;
		this.count = 1;
		this.size = 0;
	}

	public void getParamsProperty(String line) {
		
		System.out.println("LiteralMsgAnalzer_getParamsProperty");
		
		String[] props = line.split(",");
		
		String methodName = props[0];
		String lang = props[1];
		// (0,1) : securityOrder which is order Guarantee or not
		// which one is guarantee of input Order
		// which one is not (emphasis on the execute Speed)
		securityOrder =  props[2];
		// generate ProcessInvoker
		ProcessInvoker process = new ProcessInvoker(writer, this.key, lang, MethodId, methodName, null, securityOrder);
		processMap.put(MethodId, process);
		/*
		 * 1. literal Type
		 * 
		 * < Literal Type protocol > 
		 *  methodName / lang / howMany / securityOrder /  isArray / Count / Size (only array) /  END
		 */
		isArray = Integer.parseInt(props[3]);
		// if array it is the count of array params
		// if not, it is the count of literal params
		count = Integer.parseInt(props[4]);
		// if array it is the range of array params
		// if not, it is 1
		size = Integer.parseInt(props[5]);
		line = line.replace(size + ",", "");
		
		System.out.println("methodName : " + methodName);
		System.out.println("lang : " + lang);
		System.out.println("securityOrder : " + securityOrder);
		System.out.println("isArray : " + isArray);
		System.out.println("count : " + count);
		System.out.println("size : " + size);
		
		
		// prop의 0, 1, 2, 3 의  순회로 탐색하며 params generate
		
		this.end = Integer.parseInt(line.substring(line.lastIndexOf(',')+1));
		try {
			// Act to client(Device)
			writer.writeUTF(MethodId + "," + methodName + "," + "1");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getRealParms(String line) {
		
		System.out.println("LiteralMsgAnalzer_getRealParms");
		System.out.println("param : " + line);
		System.out.println("end : " + end);
		
		String[] RealParams = line.split(",");
		String filename = "*";
		if (isArray == 1) {
			// generate Array File
			filename = RpcInit.prop.get(System.getProperty("os.name").split(" ")[0])
					   .get("paramFileDir") +
					   System.getProperty("file.separator") +	
					   MethodId+".txt" ; // new 사전에 등재
			try( RandomAccessFile writer = 
					new RandomAccessFile(new File(filename), "rw"); ){
					writer.seek(writer.length());
					for (int i = 0; i < count * size; i++) {
						writer.write(RealParams[i].getBytes(Charset.defaultCharset()));
						writer.write("\r\n".getBytes(Charset.defaultCharset()));							
						int g = i+1;
						if(g % size == 0){
							writer.write("$".getBytes(Charset.defaultCharset()));
							writer.write("\r\n".getBytes(Charset.defaultCharset()));
						}
					}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			for (int i = 0; i < count; i++) {
				params.add(RealParams[i]);
			}
		}
		if (end == 1) {
			processMap.get(MethodId).setTaskQ(taskQ).setParam(params)
								    .setArray(isArray).setParamfile(filename);
			taskQ.put(MethodId, innerExeService.submit(processMap.get(MethodId)));
		}
	}

	@Override
	public void getMultiParams(String line, byte[] allbuf, int len) {}

}
