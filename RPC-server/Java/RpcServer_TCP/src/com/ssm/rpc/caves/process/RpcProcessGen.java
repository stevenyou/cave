package com.ssm.rpc.caves.process;

import java.io.PrintWriter;
import java.util.LinkedList;

public interface RpcProcessGen {
	final String filespr = System.getProperty("file.separator");
	public LinkedList<String> excute();
}
