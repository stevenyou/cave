package com.ssm.rpc.caves.concerrent;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class UdpConcerrentWorker implements Callable<String> {
	private ExecutorService innerExeService;
	// container of method Id per 1 Device
	private List<String> Methodlist;
	private Vector<Future<LinkedList<String>>> taskQ;
	private volatile Map<String, UdpProcessInvoker> processMap;
	
	


	// Constructor
	public UdpConcerrentWorker(int funtionPoolSize) {
		this.innerExeService = Executors.newFixedThreadPool(funtionPoolSize);
		this.taskQ = new Vector<Future<LinkedList<String>>>(); 
		this.processMap = new LinkedHashMap<String, UdpProcessInvoker>();
		this.Methodlist = new LinkedList<String>();
	} // Constructor End


	@Override
	public String call() throws Exception {
		System.out.println("woker_run");
		UdpProcessInvoker invoke = new UdpProcessInvoker();
		
		System.out.println(Methodlist.get(Methodlist.size()-1));
		System.out.println("Methodlist : " + Methodlist.get(Methodlist.size()-1) );
		invoke.setMethodId(Methodlist.get(Methodlist.size()-1));
		invoke.init();
		taskQ.add(innerExeService.submit(invoke));
		return "1";
	}

	public  List<String> getMethodId() {
		return Methodlist;
	}
	
	
	public UdpConcerrentWorker setMethodId( List<String> Methodlist) {
		this.Methodlist = Methodlist;
		return this;
	}



}
