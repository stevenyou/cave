package com.ssm.rpc.caves.contribution;
import java.util.HashMap;

/**
 * RpcManager
 * 
 * single tone
 * rpc data 전체를 관장
 * 
 */
public class UdpRpcManager {
	
	private final static String os = System.getProperty("os.name").split(" ")[0];
	private static volatile UdpRpcManager instance = null;
	private UdpRpcInit rpcit;
	private UdpRpcSkeletone rpcSk;
	
	// to single tone
	private UdpRpcManager(String mod){
		// 저절로 single_tone
		rpcit = new UdpRpcInit(mod);
		rpcSk = new UdpRpcSkeletone(
				Integer.parseInt(rpcit.getProp().get(os).get("devicepool")),
				Integer.parseInt(rpcit.getProp().get(os).get("portnum")), 
				Integer.parseInt(rpcit.getProp().get(os).get("functionpool")));
	}
	
	// get instance protected by race condition
	public static UdpRpcManager getInstance(){
		if(instance == null){
			synchronized (UdpRpcManager.class){
				if(instance == null){
					instance = new UdpRpcManager(os);
				}
			}
		}
		return instance;
	}
	
	
	//test
	public static void main(String[] args){
		UdpRpcManager.getInstance();
	}
	
	
}
