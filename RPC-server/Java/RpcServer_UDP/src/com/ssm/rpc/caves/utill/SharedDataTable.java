package com.ssm.rpc.caves.utill;

import java.util.LinkedHashMap;

import com.ssm.rpc.caves.contribution.UdpRpcInit;
import com.ssm.rpc.caves.contribution.UdpRpcManager;
import com.ssm.rpc.caves.contribution.UdpRpcSkeletone;
import com.ssm.rpc.caves.model.SharedData;

public class SharedDataTable {
	
	private static volatile SharedDataTable instance = null;
	public static LinkedHashMap<String, SharedData> TABLE;
	
	private SharedDataTable(){
		// 저절로 single_tone
		TABLE = new LinkedHashMap<String, SharedData>();
	}
	
	// get instance protected by race condition
	public static SharedDataTable getInstance(){
		if(instance == null){
			synchronized (UdpRpcManager.class){
				if(instance == null){
					instance = new SharedDataTable();
				}
			}
		}
		return instance;
	}
	
}
