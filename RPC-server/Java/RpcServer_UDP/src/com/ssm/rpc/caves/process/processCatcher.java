package com.ssm.rpc.caves.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

class processCatcher extends Thread {
	InputStream is;
	LinkedList<String> answer;

	public processCatcher(InputStream is) {
		this.is = is;
	}
	
	public processCatcher(InputStream is, LinkedList<String> answer) {
		this.is = is;
		this.answer = answer;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			while ((line = br.readLine()) != null){
				answer.add(line);
			}
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
	}
}