package com.ssm.rpc.caves.concerrent;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Future;
import com.ssm.rpc.caves.contribution.UdpRpcInit;
import com.ssm.rpc.caves.model.SharedData;
import com.ssm.rpc.caves.utill.SharedDataTable;
import com.ssm.rpc.caves.utill.*;
/*
 *  Mapping By Device
 *  디바이스 1개에 mapping
 */
public class UdpMsgAnalyzer {
	private DatagramSocket dsock;
	private DatagramPacket packet;
	private Map<String, String> propMap;
	private File file = null;
	private DataOutputStream dos = null;
	private int numRead = 0;
	private int curRead = 0;
	private byte[] file_dat;
	private boolean file_end;
	private int fileLimit = 0;
	private boolean flsg_enc;
	private AES aes;
	
	// Constructor
	public UdpMsgAnalyzer(DatagramSocket dsock, DatagramPacket packet) {
		this.dsock = dsock;
		this.packet = packet;
		this.propMap = new LinkedHashMap<String, String>();
		this.file_end = true;
		this.flsg_enc = true;
		this.aes = new AES();
	} // Constructor End

	public void getParam(DatagramPacket receivePacket) {
		byte[] data = receivePacket.getData(); 
		if(fileLimit == 16 && flsg_enc){
			//key setting
			byte[] key = new byte[32];
			for (int k = 0; k < key.length; k++)
				key[k] = (byte) k;
			aes.setKey(key);
			flsg_enc = false;
		}
		
		if(fileLimit == 16 && (numRead-curRead >= fileLimit)){
			byte[] tmp_16 = new byte[16];
 			for(int i = 0; i < 16; i++){
				tmp_16[i] = data[i];
			}
			byte[] tmp = aes.decrypt(tmp_16);
			String s = new String(tmp);
			data = tmp;
		}
		
		String data_s = new String(data).trim();
		if (data_s.contains("start")) {
			String  methodId = data_s.split(",")[1];
			data_s = data_s.replace("start" + "," + methodId + ",", "");
			String method = data_s.split(",")[0];
			data_s = data_s.replace(method + ",", "");
			String extension = data_s.split(",")[0];
			data_s = data_s.replace(extension + ",", "");
			String lang = data_s.split(",")[0];
			data_s = data_s.replace(lang + ",", "");
			String orderGuarantee = data_s.split(",")[0];
			data_s = data_s.replace(orderGuarantee + ",", "");
			fileLimit = Integer.parseInt(data_s.split(",")[0]);
			data_s = data_s.replace(fileLimit + ",", "");
			this.numRead = Integer.parseInt(data_s);
			file_dat = new byte[numRead];
			
			if(!propMap.containsKey(methodId)){
				SharedDataTable.getInstance().TABLE
								.put(methodId, new SharedData(new Object(),  
															      dsock, 
															      packet, 
															      lang, 
															      orderGuarantee,
															      methodId+"."+extension,
															      method));
				propMap.put(methodId, method+","+methodId+"."+extension+","+lang+","+orderGuarantee);
			} 
			System.out.println("property : " + methodId + ", "
											 + method + ", "
											 + extension + ", "
											 + lang + ", "
											 + fileLimit + ", "
											 + numRead );
			// 경로 넣어야됨
			
			this.file = new File(UdpRpcInit.prop.get(System.getProperty("os.name").split(" ")[0])
								 .get("paramFileDir") +
								 System.getProperty("file.separator") +			
							     methodId + "." + extension);
			try {
				this.dos = new DataOutputStream(new BufferedOutputStream(
						new FileOutputStream(file)));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				String returnMsg_ = "fail";
				DatagramPacket sendPacket_ = new DatagramPacket(
				returnMsg_.getBytes(), returnMsg_.getBytes().length,
				receivePacket.getAddress(), receivePacket.getPort());
				try {
					dsock.send(sendPacket_);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			String returnMsg = "ack1";
			DatagramPacket sendPacket = new DatagramPacket(
			returnMsg.getBytes(), returnMsg.getBytes().length,
			receivePacket.getAddress(), receivePacket.getPort());
			try {
				dsock.send(sendPacket);
			} catch (IOException e) {
				e.printStackTrace();
				String returnMsg_ = "fail";
				DatagramPacket sendPacket_ = new DatagramPacket(
				returnMsg_.getBytes(), returnMsg_.getBytes().length,
				receivePacket.getAddress(), receivePacket.getPort());
				try {
					dsock.send(sendPacket_);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		} else if (data_s.equalsIgnoreCase("end"
				)) {
//			System.out.println("UdpMsgAnalyzer_getParam_end");
			String returnMsg = "ack2";
			DatagramPacket sendPacket = new DatagramPacket(
			returnMsg.getBytes(), returnMsg.getBytes().length,
			receivePacket.getAddress(), receivePacket.getPort());
			try {
				dsock.send(sendPacket);
			} catch (IOException e) {
				e.printStackTrace();
				String returnMsg_ = "fail";
				DatagramPacket sendPacket_ = new DatagramPacket(
				returnMsg_.getBytes(), returnMsg_.getBytes().length,
				receivePacket.getAddress(), receivePacket.getPort());
				try {
					dsock.send(sendPacket_);
				} catch (IOException e1) {
					e1.printStackTrace();
				}			}
		} else if (file != null && curRead <= numRead && file_end) {
			try {
//				System.out.println("UdpMsgAnalyzer_getParam_file");
				int dat = 0;
				int dat_until = (numRead - curRead < fileLimit) ? numRead - curRead : fileLimit;
				for(int i = curRead; i < (curRead + dat_until); i++){
					file_dat[i] = data[dat];
					dat++;
				}
				if((curRead + dat_until) == numRead){
					dos.write(file_dat);
					file_end = false;
					try {
						dos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(numRead - curRead < fileLimit){
					curRead = curRead + (numRead - curRead);
					
				} else {
					curRead += fileLimit ;
				}
			} catch (IOException e) {
				e.printStackTrace();
				String returnMsg_ = "fail";
				DatagramPacket sendPacket_ = new DatagramPacket(
				returnMsg_.getBytes(), returnMsg_.getBytes().length,
				receivePacket.getAddress(), receivePacket.getPort());
				try {
					dsock.send(sendPacket_);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} 
		} 
	}

	public DatagramSocket getDsock() {
		return dsock;
	}

	public void setDsock(DatagramSocket dsock) {
		this.dsock = dsock;
	}
	
	public Map<String, String> getPropMap() {
		return propMap;
	}

	public void setPropMap(Map<String, String> propMap) {
		this.propMap = propMap;
	}
	
	public DatagramPacket getPacket() {
		return packet;
	}

	public void setPacket(DatagramPacket packet) {
		this.packet = packet;
	}

}
