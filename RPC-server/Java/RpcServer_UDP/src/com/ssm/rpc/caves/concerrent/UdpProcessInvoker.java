package com.ssm.rpc.caves.concerrent;

import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.LinkedList;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.print.attribute.standard.SheetCollate;

import com.ssm.rpc.caves.model.SharedData;
import com.ssm.rpc.caves.process.UDPRpcProcessJ;
import com.ssm.rpc.caves.utill.SharedDataTable;

// Mapping By Request (in Device)
// To generate the Process, which is requested by specific Device
// It handed by the ConcerrentWorker (task queue)
public class UdpProcessInvoker implements Callable<LinkedList<String>>{
	
	// to synchronize
	private String lang;
	// to file
	private FileReader reader;
	private String orderGuarantee;
	private String MethodId;
	private SharedData sharedProp;
	
	// constructor
	public UdpProcessInvoker(){
		System.out.println("UdpProcessInvoker_Constructor");
		
	}
	
	public void init(){
		this.sharedProp = SharedDataTable.getInstance().TABLE.get(this.MethodId);
		SharedDataTable.TABLE.remove(MethodId);
	}
	

	// methods
	public FileReader getReader() {
		return reader;
	}
	
	@Override
	public LinkedList<String> call() throws Exception {
		System.out.println("UdpProcessInvoker_call()");
		
		System.out.println(SharedDataTable.getInstance().TABLE.keySet());
		System.out.println(sharedProp);
		
		String method = sharedProp.getMethod();
		String paramfile = sharedProp.getFilename();
		this.lang = sharedProp.getLang();
		
		System.out.println("method : " + method);
		System.out.println("paramfile : " + paramfile);
		System.out.println("lang : " + lang);
		
		
		if(lang.equalsIgnoreCase("java")){
			// For test
			String dir = "C:\\lecture\\testcmd\\src\\test";
			LinkedList<String> answer = 
					new UDPRpcProcessJ(dir, method, paramfile, new LinkedList<String>()).excute();
			returnAnswer(answer);
		} else if (lang.equalsIgnoreCase("c")){
			/*
			 *   진기....
			 */
		} else {
		}
		return null;
	}
	
	public String getMethodId() {
		return MethodId;
	}
	
	
	public UdpProcessInvoker setMethodId(String methodId) {
		MethodId = methodId;
		return this;
	}

	public void returnAnswer(LinkedList<String> answer) {
		for(int i = 0; i < answer.size(); i++){
			DatagramPacket sendPacket_ = new DatagramPacket(
			answer.get(i).getBytes(), answer.get(i).getBytes().length,
			sharedProp.getPacket().getAddress(), sharedProp.getPacket().getPort());
			try {
				sharedProp.getDsock().send(sendPacket_);
			} catch (IOException e) {
				e.printStackTrace();
				String rt = "fail";
				DatagramPacket sendPacket = new DatagramPacket(rt.getBytes(), rt.getBytes().length,
				sharedProp.getPacket().getAddress(), sharedProp.getPacket().getPort());
				try {
					sharedProp.getDsock().send(sendPacket);
				} catch (IOException e2) {
					e.printStackTrace();
				}
			}
		}
		
		
	}
	

}
