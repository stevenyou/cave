package com.ssm.rpc.caves.model;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class SharedData {
	
	private Object key;

	private DatagramSocket dsock;
	private DatagramPacket packet; 
	private String lang;
	private String orderGuarantee;
	private String filename;
	private String method;
	
	public SharedData(Object key, 
					  DatagramSocket dsock,
					  DatagramPacket packet, 
					  String lang,
					  String orderGuarantee,
					  String filename,
					  String method) {
		this.key = key;
		this.dsock = dsock;
		this.packet = packet;
		this.lang = lang;
		this.orderGuarantee = orderGuarantee;
		this.filename = filename;
		this.method = method;
	}
	
	public Object getKey() {
		return key;
	}


	public void setKey(Object key) {
		this.key = key;
	}


	public DatagramSocket getDsock() {
		return dsock;
	}


	public void setDsock(DatagramSocket dsock) {
		this.dsock = dsock;
	}


	public DatagramPacket getPacket() {
		return packet;
	}


	public void setPacket(DatagramPacket packet) {
		this.packet = packet;
	}


	public String getLang() {
		return lang;
	}


	public void setLang(String lang) {
		this.lang = lang;
	}


	public String getOrderGuarantee() {
		return orderGuarantee;
	}


	public void setOrderGuarantee(String orderGuarantee) {
		this.orderGuarantee = orderGuarantee;
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


	public String getMethod() {
		return method;
	}


	public void setMethod(String method) {
		this.method = method;
	}

	
}
