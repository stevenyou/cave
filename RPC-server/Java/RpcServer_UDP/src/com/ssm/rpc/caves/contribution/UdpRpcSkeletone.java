package com.ssm.rpc.caves.contribution;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.ssm.rpc.caves.concerrent.UdpConcerrentWorker;
import com.ssm.rpc.caves.concerrent.UdpMsgAnalyzer;

public class UdpRpcSkeletone {
	// Device(Client)처리 개수
	// 같은 Device 에서 온 task들은 (return 전에 또 온 것들)
	// 내부의Task queue로 관리
	private ExecutorService exeService;
	private Object key;
	private Map<String, UdpMsgAnalyzer> devicelist = new LinkedHashMap<String, UdpMsgAnalyzer>();
	private int funtionPoolSize = 1;
	private Map<String, Future<String>> deviceThread;
	private Map<String, UdpConcerrentWorker> deviceTable;
	
	public UdpRpcSkeletone(int poolSize, int portNum, int funtionPoolSize) {
		this.key = new Object();
		this.exeService = Executors.newFixedThreadPool(poolSize);
		this.funtionPoolSize = funtionPoolSize;
		this.deviceThread = new LinkedHashMap<String, Future<String>>();
		this.deviceTable = new LinkedHashMap<String, UdpConcerrentWorker>(); 
		runServ(portNum);
	}

	public void runServ(int portNum) {
		try {
			// portNum은 추후 변환
			DatagramSocket dsock = new DatagramSocket(portNum);
			System.out.println("UDP : Wait for Device request.");
			while (true) {
//				System.out.println("< while_in_UdpRpcSkeletone >");
				synchronized (key) {
					DatagramPacket receivePacket = new DatagramPacket(
							new byte[65508], 65508);
					dsock.receive(receivePacket);
					String Id = receivePacket.getAddress()
							.getHostAddress() + "#" + receivePacket.getPort();
//					System.out.println("Id :" + Id);
					String canExe = new String(receivePacket.getData()).trim();
					if(canExe.contains("exe_")){
						System.out.println("runServ_exe");
						if(deviceThread.containsKey(Id)){
							System.out.println("if");
							deviceTable.get(Id).getMethodId().add(canExe.split("_")[1]);
							exeService.submit(deviceTable.get(Id));
						} else {
							// UdpConcerrentWorker ---> Device 1개당 하나
							System.out.println("else");
							UdpConcerrentWorker worker = new UdpConcerrentWorker(this.funtionPoolSize);
							deviceTable.put(Id, worker);
							deviceTable.get(Id).getMethodId().add(canExe.split("_")[1]);
							deviceThread.put(Id, exeService.submit(worker));
							devicelist.remove(Id);
						}
					} else if (canExe.equalsIgnoreCase("shutdown")){
						deviceThread.remove(Id);
						deviceTable.remove(Id);
						devicelist.remove(Id);
					} else {
//						System.out.println("runServ_getParams");
						if (devicelist.containsKey(Id)) {
							devicelist.get(Id).getParam(receivePacket);
						} else {
							devicelist.put(Id, new UdpMsgAnalyzer(dsock, receivePacket));
							devicelist.get(Id).getParam(receivePacket);
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
