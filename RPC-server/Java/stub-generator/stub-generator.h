/*
 * stub-generator.h
 *
 *  Created on: 2014. 8. 24.
 *      Author: jk
 */

#ifndef _STUB_GENERATOR_H_
#define _STUB_GENERATOR_H_

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <unistd.h>
#include <sys/stat.h>

#define SEPERATOR ','
#define TYPE_NAME_START '['
#define TYPE_NAME_END ']'

using namespace std;

class VariableType{
private:
	bool unsigned_flag;
	bool struct_flag;
	string type_name;
	bool pointer_flag;

	/* pointer_flag가 true인 경우에만 사용 */
	string type_string;
	unsigned int index;
public:
	VariableType();
	VariableType(string &);

	void operator=(VariableType &);
	bool isPointerFlag() const { return pointer_flag; }
	const string& getTypeName() const { return type_name; }

	string getTypeString();
	string getDeclaration();
	string getDeclaration(unsigned int);
	string getAllocation();
	string getFormat();
	string getAssignment();
	string getFree();
};

class StubGenerator{
private:
	string info_dir;

	string project_name;
	string project_dir_path;
	string relative_path;
	string target_file_name;
	string target_file_path;
	string target_function_name;

	vector<VariableType> param_types;
	VariableType return_type;

	string stub_source_code_path;
	string stub_file_path;

	string compile_opt;

	int main_off;

	int ignoreGap(fstream &, char &);
	int ignoreString(fstream &, char &);
	int ignoreComment(fstream &, char &);
	int ignoreBrace(fstream &, char &);
	int ignoreParentheses(fstream &, char &);
	void restoreMain();
public:
	StubGenerator(int, char **);
	string getProjectListPath();
	string getStubDirPath();
	string getStubSourceCodePath();
	string getLibDirPath();
	string getProjectDirPath();
	string getTargetFilePath();
	bool findFunction(fstream &, string);
	bool checkFunctionExist();
	bool checkProjectList();
	bool generateStub();
};

#endif /* _STUB_GENERATOR_H_ */
