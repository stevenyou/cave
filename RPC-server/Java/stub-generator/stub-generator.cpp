/**
 * @file	stub-generator.c
 * @author	JK Kim<copy_x@naver.com>
 *
 * ISSUE
 * 이미지, 사운드, 영상은 따로 분류. 리턴을 파일로.
 */
#include "stub-generator.h"

int StubGenerator::ignoreGap(fstream& fs, char &ch){
	int get_count = 0;
	do {
		ch = fs.get();
		get_count++;
	} while (ch == ' ' || ch == '\t' || ch == '\n');
	return get_count;
}

int StubGenerator::ignoreString(fstream& fs, char &ch){
	int get_count = 0;
	do {
		ch = fs.get();
		get_count++;
	} while (ch != '"');
	return get_count;
}

int StubGenerator::ignoreComment(fstream& fs, char &ch){
	int get_count = 0;
	ch = fs.get();
	if (ch == '/') {			/* 한 줄 주석 */
		do {
			ch = fs.get();
			get_count++;
		} while (ch != '\n');
	} else if (ch == '*') { /* 여러 줄 주석 */
		do {
			ch = fs.get();
			get_count++;
			if (ch == '*') {
				ch = fs.get();
				get_count++;
			}
		} while (ch != '/');
	}
	return get_count;
}

int StubGenerator::ignoreBrace(fstream& fs, char &ch){
	int get_count = 0;
	do {
		ch = fs.get();
		get_count++;
	} while (ch != '}');
	return get_count;
}

int StubGenerator::ignoreParentheses(fstream& fs, char &ch){
	int get_count = 0;
	do {
		ch = fs.get();
		get_count++;
	} while (ch != ')');
	return get_count;
}

void StubGenerator::restoreMain(){
	fstream source(this->target_file_path.c_str(), fstream::in | fstream::out);
	source.seekg(this->main_off, fstream::beg);
	source.put('n');
}

VariableType::VariableType(){
	this->unsigned_flag = false;
	this->struct_flag = false;
	this->pointer_flag = false;
	this->index = 0;
}
VariableType::VariableType(string &type_str){
	this->unsigned_flag = false;
	this->struct_flag = false;
	this->pointer_flag = false;
	this->index = 0;

	while (type_str[0] != SEPERATOR && type_str[0] != '\0') {
		switch (type_str[0]) {
			case 'U':
				 this->unsigned_flag = true; break;
			case 'V':
				this->type_name += "void"; break;
			case 'C':
				this->type_name += "char"; break;
			case 'S':
				if (type_str[1] == 'T') {
					this->struct_flag = true;

					if (type_str[2] != TYPE_NAME_START) {
						cout << "There is no struct name\n";
						exit(1);
					} else if (type_str[3] == TYPE_NAME_END) {
						cout << "There is no struct name\n";
						exit(1);
					} else {
						type_str.erase(0, 3);
					}

					while (type_str[0] != TYPE_NAME_END) {
						this->type_name += type_str[0];
						type_str.erase(0, 1);
					}
				} else {
					this->type_name += "short";
				}
				break;
			case 'I':
				this->type_name += "int"; break;
			case 'L':
				this->type_name += "long";

				if (type_str[1] == 'L') {
					this->type_name += " long";
					type_str.erase(0, 1);
				}
				else if (type_str[1] == 'D') {
					this->type_name += " double";
					type_str.erase(0, 1);
				}
				break;
			case 'F':
				this->type_name += "float"; break;
			case 'D':
				this->type_name += "double"; break;
			case 'P':
			case 'A':
				this->pointer_flag = true; break;
			case 'T':
				if (type_str[1] != TYPE_NAME_START) {
					cout << "There is no typedef name\n";
					exit(1);
				} else if (type_str[2] == TYPE_NAME_END) {
					cout << "There is no typedef name\n";
					exit(1);
				} else {
					type_str.erase(0, 2);
				}

				while (type_str[0] != TYPE_NAME_END) {
					this->type_name += type_str[0];
					type_str.erase(0, 1);
				}
				break;
			default:
				cout << "\'" << type_str[0] << "\' is wrong type-character" << endl;
				exit(1);
		}
		type_str.erase(0, 1);
	}
	type_str.erase(0, 1);
}

void VariableType::operator=(VariableType &vt){
	this->unsigned_flag = vt.unsigned_flag;
	this->type_name = vt.type_name;
	this->pointer_flag = vt.pointer_flag;
}

string VariableType::getTypeString(){
	/* 두 번째 호출 부터 저장되어 있는 값 반환 */
	if (!this->type_string.empty())
		return this->type_string;

	string type_string;

	if (this->unsigned_flag) {
		type_string += "unsigned ";
	} else if (this->struct_flag) {
		type_string += "struct ";
	}

	type_string += this->type_name + " ";

	this->type_string = type_string;
	return type_string;
}

string VariableType::getDeclaration(){
	string decl, type_string = this->getTypeString();

	decl += type_string;

	if (this->pointer_flag) {
		decl += "*";
	}

	return decl;
}

string VariableType::getDeclaration(unsigned int index){
	string decl = this->getDeclaration();
	ostringstream ostr;

	this->index = index;
	ostr << index;

	decl += "arg" + ostr.str() + ";\n";

	return decl;
}

string VariableType::getAllocation(){
	string alloc;
	ostringstream index, argv_count_index;

	if (!this->pointer_flag)
		return alloc;

	index << this->index;
	argv_count_index << this->index + 1;

	alloc += "fscanf(fp, \"%d\\n\", &number_of_elements);\n";
	alloc += "\targ" + index.str();
	alloc += " = (" + this->type_string + " *)malloc(sizeof("
			+ this->type_string + ") * number_of_elements";
	if (this->type_name == "char" && this->pointer_flag)
		alloc += " + 1";

	alloc += ");\n";

	return alloc;
}

string VariableType::getFormat(){
	string format;

	if (this->struct_flag) {
		format += "/* User Defined */";
	} else if (this->type_name == "char") {
		if (this->pointer_flag)
			format += "%s";
		else
			format += "%c";
	} else if (this->type_name == "short") {
		if (this->unsigned_flag)
			format += "%hu";
		else
			format += "%hd";
	} else if (this->type_name == "int") {
		if (this->unsigned_flag)
			format += "%u";
		else
			format += "%d";
	} else if (this->type_name == "long") {
		if (this->unsigned_flag)
			format += "%lu";
		else
			format += "%ld";
	} else if (this->type_name == "float") {
		format += "%f";
	} else if (this->type_name == "double") {
		format += "%lf";
	} else {
		format += "/* User Defined */";
	}

	return format;
}

string VariableType::getAssignment(){
	string assign;
	ostringstream index;

	index << this->index;

	if (this->pointer_flag) {
		/* 동적 할당 */
		assign += this->getAllocation();
		if (this->type_name != "char")
			assign += "\tfor (i = 0; i < number_of_elements; i++) {\n\t";
	}

	/* 일반 변수 */
	assign += "\tfscanf(fp, \"" + this->getFormat();

	if (this->pointer_flag) {
		assign += "\\n\", arg" + index.str();

		if (this->type_name != "char") {
			assign += "[i]);\n";
			assign += "\t}\n";
		} else {
			assign += ");\n";
		}
	} else {
		assign += "\\n\", &arg" + index.str() + ");\n";
	}

	return assign;
}

string VariableType::getFree(){
	string free;
	ostringstream index;

	index << this->index;

	if (this->pointer_flag) {
		free += "\tfree(arg" + index.str() + ");\n";
	}

	return free;
}

StubGenerator::StubGenerator(int argc, char **argv){
	/* 환경 변수에서 cave-rpc-info 경로 추출 */
	char *path = getenv("CAVE_RPC_INFO_PATH");

	/* path의 디렉토리 존재 확인 */
	if (!path || access(path, F_OK)) {
		cout << "cave-rpc-info directory does not exist" << endl
			<< "Please, check if cave-rpc-server is installed" << endl;
		exit(1);
	}

	this->info_dir = path;

	/* 프로그램 arguments 수 확인 */
	if (argc < 6) {
		cout << "Check if you input right arguments" << endl;
		exit(1);
	}


	/* 프로그램 arguments로부터 각 변수들을 추출 */
	this->project_name = argv[1];
	this->relative_path = argv[2];

	unsigned int i = this->relative_path.size() - 1;
	while (this->relative_path[i] != '/') {
		this->target_file_name = this->relative_path[i] + this->target_file_name;
		if (i == 0) break;
		i--;
	}

	this->target_function_name = argv[3];

	string type_str = argv[4];
	while (type_str[0]) {
		VariableType vt(type_str);
		this->param_types.push_back(vt);
	}

	type_str = argv[5];
	VariableType vt(type_str);
	this->return_type = vt;

	for (int i = 6; i < argc; i++) {
		string opt = argv[i];
		this->compile_opt += " " + opt;
	}

	this->main_off = 0;
}

string StubGenerator::getProjectListPath(){
	string project_list_path = this->info_dir + "/project-list";

	/* 파일 존재 확인 */
	if (access(project_list_path.c_str(), F_OK)) {
		cout << project_list_path << " does not exist" << endl;
		exit(1);
	}

	return project_list_path;
}

string StubGenerator::getStubDirPath(){
	string stub_dir_path = this->info_dir + "/stub";

	/* 디렉토리 존재 확인 */
	if (access(stub_dir_path.c_str(), F_OK)) {
		cout << stub_dir_path << " does not exist" << endl;
		exit(1);
	}
	return stub_dir_path;
}

string StubGenerator::getStubSourceCodePath(){
	string stub_file_path = this->getStubDirPath() + "/" + this->project_name;

	if (access(stub_file_path.c_str(), F_OK)) {
		if (mkdir(stub_file_path.c_str(), 0755)) {
			cout << "cannot make directory: " << stub_file_path << endl;
			cout << "please, try again" << endl;
			exit(1);
		}
	}

	stub_file_path += '/';

	for (unsigned int i = 0; i < this->relative_path.size(); i++) {
		stub_file_path += this->relative_path[i];

		if ((this->relative_path[i] == '/' || i == this->relative_path.size() - 1) && access(stub_file_path.c_str(), F_OK)) {
			if (mkdir(stub_file_path.c_str(), 0755)) {
				cout << "cannot make directory: " << stub_file_path << endl;
				cout << "please, try again" << endl;
				exit(1);
			}
		}
	}

	this->stub_file_path = stub_file_path + "/" + this->target_function_name;
	this->stub_source_code_path = this->stub_file_path + ".c";
	return this->stub_source_code_path;
}

string StubGenerator::getLibDirPath(){
	string lib_dir_path = this->info_dir + "/lib";

	/* 디렉토리 존재 확인 */
	if (access(lib_dir_path.c_str(), F_OK)) {
		cout << lib_dir_path << " does not exist" << endl;
		exit(1);
	}
	return lib_dir_path;
}

/* 프로젝트 경로 추출 */
string StubGenerator::getProjectDirPath(){
	/* 프로젝트 경로 존재 확인 */
	if (access(project_dir_path.c_str(), F_OK)) {
		cout << project_dir_path << " does not exist" << endl;
		exit(1);
	}
	
	return project_dir_path;
}

string StubGenerator::getTargetFilePath(){
	string target_file_path = getProjectDirPath() + "/" + this->relative_path;
	cout << target_file_path << endl;
	/* 파일 존재 확인 */
	if (access(target_file_path.c_str(), F_OK)) {
		cout << target_file_path << " does not exist" << endl;
		exit(1);
	}

	return target_file_path;
}

/* 파일에서 함수 이름 검색 */
bool StubGenerator::findFunction(fstream &fs, string function_name){
	unsigned int index = 0;
	int get_count = 0;
	char ch = '\0', old = '\0';
	bool main_flag = (function_name == "main");
	function_name = function_name + "(){";

	while (fs.good()) {
		if (index == (function_name.size() - 3) || index == (function_name.size() - 1)) {
			get_count -= this->ignoreGap(fs, ch);
		} else if (index == (function_name.size() - 2)) {
			get_count -= this->ignoreParentheses(fs, ch);
		} else if (index == function_name.size()) {
			if (main_flag) {
				get_count += 3;
				fs.seekg(get_count, fstream::cur);
				this->main_off = fs.tellg();
				fs.put('_');
			}
			return true;
		} else {
			old = ch;
			ch = fs.get();
			get_count--;
		}

		if (ch == '"') {							/* 문자열 무시 처리 */
			this->ignoreString(fs, ch);
		} else if (ch == '/') { 					/* 주석 무시 처리 */
			this->ignoreComment(fs, ch);
		} else if (ch == '{' && index != (function_name.size() - 1)) {	/* 중괄호 무시 처리 */
			this->ignoreBrace(fs, ch);
		} else	if (ch == function_name[index]) {
			if (index == 0 && old != ' ' && old != '\t' && old != '\n')
				continue;
			index++;
		}
		else {
			get_count = index = 0;
		}
	}
	return false;
}

/* 함수 존재 확인 */
bool StubGenerator::checkFunctionExist(){
	/* Target function이 있는 파일 open */
	this->target_file_path = getTargetFilePath();
	fstream source(this->target_file_path.c_str(), fstream::in | fstream::out);

	if (this->target_function_name != "main" && this->findFunction(source, "main")) {
		cout << this->target_file_name << " has \"main\" function" << endl;
		cout << "change from \"main\" to \"mai_\" temporarily" << endl;
	}

	source.close();
	source.open(this->target_file_path.c_str(), fstream::in | fstream::out);

	if (this->findFunction(source, this->target_function_name)) {
		cout << this->target_file_name << " has " << this->target_function_name << " function" << endl;
		if (this->target_function_name == "main") {
			cout << "change from \"main\" to \"mai_\" temporarily" << endl;
			this->target_function_name = "mai_";
		}
		source.close();
		return true;
	} else {
		cout << this->target_function_name << " does not exist in " << this->target_file_path << endl;
		source.close();
		return false;
	}
}

bool StubGenerator::checkProjectList(){
	ifstream project_list(this->getProjectListPath().c_str());
	string project_name, project_path;

	/* 프로젝트 이름 검색 */
	while (project_list.good()) {
		project_list >> project_name >> project_path;
		
		if (project_name == this->project_name) {
			cout << project_name << " : " << project_path << endl;
			this->project_dir_path = project_path;
			project_list.close();

			return true;
		}
	}
	cout << this->project_name << " is not registered" << endl;
	project_list.close();
	return false;
}

/* Stub 파일 생성 */
bool StubGenerator::generateStub(){
	ofstream stub(this->getStubSourceCodePath().c_str());

	stub << "/* Generated by CAVE */" << endl;
	stub << "#include <stdio.h>" << endl;
	stub << "#include <stdlib.h>" << endl;

	/* Target file을 절대 경로로 Include */
	stub << "#include \"" << this->target_file_path << "\"" << endl;
	stub << "int main(int argc, char **argv){" << endl;
	stub << "\tFILE *fp;" << endl;
	stub << "\tint i, number_of_elements;" << endl;

	/* Return 변수 선언 */
	if (this->return_type.getTypeName() != "void")
		stub << "\t" << this->return_type.getDeclaration() << "ret;" << endl;
	stub << endl;

	/* Argument 변수 선언 */
	stub << "\t/* 변수 선언 */" << endl;
	for (unsigned int i = 0; i < this->param_types.size(); i++) {
		stub << "\t" << this->param_types[i].getDeclaration(i + 1);
	}
	stub << endl;

	/* Pointer 동적 할당 및 stub argument 변환 및 저장 */
	stub << "\t/* Pointer 동적 할당 및 stub Argument 변환 및 저장 */" << endl;
	stub << "\tfp = fopen(argv[1], \"r\");" << endl << endl;
	for (unsigned int i = 0; i < this->param_types.size(); i++) {
		stub << "\t" << this->param_types[i].getAssignment();
	}
	stub << "\n\tfclose(fp);" << endl << endl;

	/* Function call */
	stub << "\t/* 함수 호출 */" << endl;

	stub << "\t";
	if (this->return_type.getTypeName() != "void")
		stub << "ret = ";
	stub << this->target_function_name << "(";

	/* Function argument 삽입 */
	for (unsigned int i = 0; i < this->param_types.size(); i++) {
		if (i) {
			stub << ", ";
		}
		stub << "arg" << i + 1;
	}

	stub << ");" << endl << endl;

	/* return out 부분 */
	if (this->return_type.getTypeName() != "void") {
		stub << "\t/* RPC 서버로 반환 */" << endl;
		stub << "\tprintf(\"$@#" + this->return_type.getFormat();
		stub << "\\n\", ret);" << endl << endl;
	}

	/* 변수 Free */
	stub << "\t/* 포인터 변수 해제 */" << endl;
	for (unsigned int i = 0; i < this->param_types.size(); i++) {
		stub << this->param_types[i].getFree();
	}
	stub << endl << "\treturn 0;" << endl;
	stub << "}";

	stub.close();

	/* 사용자가 컴파일하기 위한 정보를 .[function_name]_compile_info 에 저 */
	string compile_info_path = this->getStubDirPath() + "/" + this->project_name
							+ "/" + this->relative_path + "/." + this->target_function_name + "_compile_info";
	cout << compile_info_path << endl;
	ofstream compile_info(compile_info_path.c_str());

	compile_info << this->target_file_path << endl;
	compile_info << this->stub_file_path << endl;
	compile_info << this->compile_opt;

	compile_info.close();

	/* Stub 소스 코드 컴파일 */
	string compile_cmd("gcc -o " + this->stub_file_path + " "
							+ this->stub_source_code_path + " " + this->compile_opt);

	if (system(compile_cmd.c_str()))
		cout << "compile is failed" << endl;
	else
		cout << "compile is success" << endl;
	/* 일시적으로 수정한 main을 복구 */
	if (main_off)
		this->restoreMain();
	return true;
}

/**
 * @fn	main
 * @param	argc Argument의 수
 * @param	argv 프로젝트 이름, 파일 경로, 함수 이름, 매개변수 타입, 반환 타입
 * @return 	실행 상태
 */
int main(int argc, char **argv){
	StubGenerator stub_gen(argc, argv);

	/* project-list에서 프로젝트 이름이 있는지 확인 */
	if (stub_gen.checkProjectList() && stub_gen.checkFunctionExist()) {
		stub_gen.generateStub();
	}
	return 0;
}
