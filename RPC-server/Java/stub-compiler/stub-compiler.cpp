/*
 * stub_compile.cpp
 *
 *  Created on: 2014. 9. 2.
 *      Author: jk
 */

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <linux/limits.h>

using namespace std;

int main_off;

int ignoreGap(fstream& fs, char &ch){
	int get_count = 0;
	do {
		ch = fs.get();
		get_count++;
	} while (ch == ' ' || ch == '\t' || ch == '\n');
	return get_count;
}

int ignoreString(fstream& fs, char &ch){
	int get_count = 0;
	do {
		ch = fs.get();
		get_count++;
	} while (ch != '"');
	return get_count;
}

int ignoreComment(fstream& fs, char &ch){
	int get_count = 0;
	ch = fs.get();
	if (ch == '/') {			/* 한 줄 주석 */
		do {
			ch = fs.get();
			get_count++;
		} while (ch != '\n');
	} else if (ch == '*') { /* 여러 줄 주석 */
		do {
			ch = fs.get();
			get_count++;
			if (ch == '*') {
				ch = fs.get();
				get_count++;
			}
		} while (ch != '/');
	}
	return get_count;
}

int ignoreBrace(fstream& fs, char &ch){
	int get_count = 0;
	do {
		ch = fs.get();
		get_count++;
	} while (ch != '}');
	return get_count;
}

int ignoreParentheses(fstream& fs, char &ch){
	int get_count = 0;
	do {
		ch = fs.get();
		get_count++;
	} while (ch != ')');
	return get_count;
}

void restoreMain(string &target_file_path){
	fstream source(target_file_path.c_str(), fstream::in | fstream::out);
	source.seekg(main_off, fstream::beg);
	source.put('n');
}

/* 파일에서 함수 이름 검색 */
bool findFunction(fstream &fs, string function_name){
	unsigned int index = 0;
	int get_count = 0;
	char ch = '\0', old = '\0';
	bool main_flag = (function_name == "main");
	function_name = function_name + "(){";

	while (!fs.eof()) {
		if (index == (function_name.size() - 3) || index == (function_name.size() - 1)) {
			get_count -= ignoreGap(fs, ch);
		} else if (index == (function_name.size() - 2)) {
			get_count -= ignoreParentheses(fs, ch);
		} else if (index == function_name.size()) {
			if (main_flag) {
				get_count += 3;
				fs.seekg(get_count, fstream::cur);
				main_off = fs.tellg();
				fs.put('_');
			}
			return true;
		} else {
			old = ch;
			ch = fs.get();
			get_count--;
		}

		if (ch == '"') {							/* 문자열 무시 처리 */
			ignoreString(fs, ch);
		} else if (ch == '/') { 					/* 주석 무시 처리 */
			ignoreComment(fs, ch);
		} else if (ch == '{' && index != (function_name.size() - 1)) {	/* 중괄호 무시 처리 */
			ignoreBrace(fs, ch);
		} else	if (ch == function_name[index]) {
			if (index == 0 && old != ' ' && old != '\t' && old != '\n')
				continue;
			index++;
		}
		else {
			get_count = index = 0;
		}
	}
	return false;
}

void checkMainExist(string &target_file_path){
fstream source(target_file_path.c_str(), fstream::in | fstream::out);

	if (findFunction(source, "main")) {
		cout << target_file_path << " has \"main\" function" << endl;
		cout << "change from \"main\" to \"mai_\" temporarily" << endl;
		source.seekg(0, fstream::beg);
	}
}
int main(int argc, char **argv){
	string target_function_name = argv[1];
	target_function_name = "." + target_function_name + "_compile_info";

	ifstream compile_info(target_function_name.c_str());

	string target_file_path, stub_file_path, compile_opt, buffer;
	compile_info >> target_file_path;
	compile_info >> stub_file_path;

	cout << target_file_path << endl;
	cout << stub_file_path << endl;
	while (compile_info.good()) {
		compile_info >> buffer;
		compile_opt += " " + buffer;
	}

	string compile_cmd("gcc -o " + stub_file_path + " "
							+ stub_file_path + ".c" + compile_opt);
	//cout << compile_cmd << endl;

	checkMainExist(target_file_path);

	if (system(compile_cmd.c_str()))
		cout << "compile is failed" << endl;
	else
		cout << "compile is success" << endl;

	restoreMain(target_file_path);

	compile_info.close();

	return 0;
}
