package com.ssm.rpc.caves.generate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Random;

import com.ssm.rpc.caves.utill.gitClone;

public class StubGenWorker implements Runnable {

	private String sp;
	private BufferedReader br;
	private String property[];
	private String girDir;
	private String glistDir;
	private String clonedir;

	public StubGenWorker(Socket sock, String gitDir, String glistDir,
			String clonedir) {
		this.sp = System.getProperty("file.separator");
		this.girDir = gitDir;
		this.glistDir = glistDir;
		this.clonedir = clonedir;
		try {
			this.br = new BufferedReader(new InputStreamReader(
					sock.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		String line = null;
		String cloneDir = null;

		try {
			while ((line = br.readLine()) != null) {
				// << url/project,targetFileName,targetFuntionName >>
				// 1. git clone() using with the url, project name
				// 2. pasing the readmd file get the property to generate the Stub
				// 3. generate the stub, if c, make another process
				// [0] = url
				// [1] = projectName/Package
				// [2] = fileName
				// [3] = FunctionName
				property = line.split(",");
				cloneDir = property[2] +(property[2].length() + property[3].length()%(new Random().nextInt(10)+1)) + property[3];
				for (int i = 0; i < property.length; i++) {
					System.out.println(" *prop : " + property[i]);
				}
				
//				param1 : Remote url, param2 : Directory Name, which cloned file saved 
				gitClone.cloneRemote(property[0], cloneDir);
				
				// 여기서 부터 그 코드를 기반으로 stub 실행
				
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
