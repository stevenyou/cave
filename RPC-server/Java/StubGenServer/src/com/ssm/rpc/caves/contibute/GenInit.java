package com.ssm.rpc.caves.contibute;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GenInit {
	
	public static Map<String, Map<String, String>> commands;
	public static Map<String, Map<String, String>> prop;
	private final String filespr = System.getProperty("file.separator");
	private final String usrdir = System.getProperty("user.dir");
	
	public GenInit(String mod){
		commands = new HashMap<String, Map<String, String>>();
		prop = new HashMap<String, Map<String, String>>();
		init(mod);
	}
	
	public synchronized Map<String,  Map<String, String>> getCommands(){
		return commands;
	}
	
	public synchronized Map<String,  Map<String, String>> getProp(){
		return prop;
	}
	
	private void init(String mod){
		String path = usrdir + filespr + "src" + filespr +"com" + filespr 
				      + "ssm" + filespr + "rpc" + filespr + "caves" 
				      + filespr + "commands" + filespr + mod + ".txt";
		try(BufferedReader reader = new BufferedReader(
				new InputStreamReader( 
						new FileInputStream(path), System.getProperty("file.encoding") )); ){ 
						// commands 추가
			Map<String, String> cmap = new HashMap<String, String>();
			String line = null;
			while((line = reader.readLine()) != null ) {
				cmap.put(line.split("=")[0], line.split("=")[1]);
			}
			commands.put(mod, cmap); 
			//test
			Set<String> test = cmap.keySet();
			Iterator<String> itr = test.iterator();
			while(itr.hasNext()){
				String key = itr.next();
				System.out.print(key + " = ");
				System.out.println(cmap.get(key));
			}
			// test end
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		path = usrdir + filespr + "src" + filespr +"com" + filespr 
		       + "ssm" + filespr + "rpc" + filespr + "caves" 
		       + filespr + "property" + filespr + "property.txt";
		try(BufferedReader reader = new BufferedReader(
				new InputStreamReader( 
						new FileInputStream(path), System.getProperty("file.encoding") )); ){ 
						// commands 추가
			String line = null;
			Map<String, String> pmap = new HashMap<String, String>();
			while((line = reader.readLine()) != null ) {
				if(line.startsWith("ev")){
					String evprop = ""; 
					String temp = "";
					String target = line.split("=")[1];
					List<String> ev = new LinkedList<String>();
					for(int i = 0; i < target.length(); i++){
						int k = (int)target.charAt(i);
						if(k != 92 && k != 47){
							temp += target.charAt(i);
						} else {
							ev.add(temp);
							temp="";
						}
						if(i == target.length()-1){
							ev.add(temp);
						}
					}
					for(int i = 0; i < ev.size(); i++){
						evprop += ev.get(i) + filespr;
					}
					evprop = evprop.substring(0,evprop.lastIndexOf(filespr));
					pmap.put("ev", evprop);
				} else {
					pmap.put(line.split("=")[0], line.split("=")[1]);
				}
			}
			prop.put(mod, pmap); 
			//test
			Set<String> test = pmap.keySet();
			Iterator<String> itr = test.iterator();
			while(itr.hasNext()){
				String key = itr.next();
				System.out.print(key + " = ");
				System.out.println(pmap.get(key));
			}
			// test end
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
