package com.ssm.rpc.caves.contibute;
import java.util.HashMap;

import com.ssm.rpc.caves.generate.GenStubServer;

/**
 * RpcManager
 * 
 * single tone
 * rpc data 전체를 관장
 * 
 */
public class GenManager {
	
	private final static String os = System.getProperty("os.name").split(" ")[0];
	private static volatile GenManager instance = null;
	private GenInit rpcit;
	private int stubport;
	private String gitdir;
	private GenStubServer genStSer; 
	// to single tone
	private GenManager(String mod){
		// 저절로 single_tone
		rpcit = new GenInit(mod);
		genStSer = new GenStubServer(
				Integer.parseInt(rpcit.getProp().get(os).get("stubpoolsize")),
				Integer.parseInt(rpcit.getProp().get(os).get("stubport")), 
								 rpcit.getProp().get(os).get("gitdir"), 
								 rpcit.getProp().get(os).get("genStSer"), 
								 rpcit.getProp().get(os).get("clonedir"));
		}
	
	// get instance protected by race condition
	public static GenManager getInstance(){
		if(instance == null){
			synchronized (GenManager.class){
				if(instance == null){
					instance = new GenManager(os);
				}
			}
		}
		return instance;
	}
	
	
	//test
	public static void main(String[] args){
		GenManager.getInstance();
	}
	
	
}
