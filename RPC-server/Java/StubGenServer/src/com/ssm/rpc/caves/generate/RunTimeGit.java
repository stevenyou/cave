package com.ssm.rpc.caves.generate;

public class RunTimeGit {
	
	public Process process = null;
	public Runtime runTime;
	public String cmd[];
	
	public RunTimeGit(String gitdir){
	
	runTime = Runtime.getRuntime();

	cmd = new String[3];
	cmd[0] = "cmd.exe";
	cmd[1] = "/C";
	cmd[2] = "cd " + gitdir + "&&sh.exe --login -i";
	
	}
	
}
