package com.ssm.rpc.caves.utill;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import com.ssm.rpc.caves.contibute.GenInit;
import com.ssm.rpc.caves.utill.CheckGlistSer;
import com.ssm.rpc.caves.utill.CheckInit;

public class gitClone {

	public static void cloneRemote(String url, String DirName) {
		String remotePath = url;
		url = url.substring(url.indexOf("//")+2);
		url = url.substring(0, url.lastIndexOf("/"));
		url = url.substring(0, url.lastIndexOf("/"));
		
		CheckInit ck = new CheckInit();
		if(!ck.isAlreadyCheckSSH(url)){
		
			System.out.println("파워 퍼프 걸 ㅠㅠㅠㅠㅠ");
			
			String cmd[] = new String[3];
			String entre = System.getProperty("line.separator");
			cmd[0] = "cmd.exe";
			cmd[1] = "/C";
			cmd[2] = "cd " + "C:\\Program Files (x86)\\Git\\bin"
					+ "&&sh.exe --login -i";
			Runtime runTime = Runtime.getRuntime();
			Process process = null;
			processCatcher_ gbe = null;
			try {
				process = runTime.exec(cmd);
				PrintWriter out = new PrintWriter(process.getOutputStream());
				gbe = new processCatcher_(process.getErrorStream(), out, url);
				gbe.start();
				while (true) {
					if (!gbe.isAlive()) {
						// process.waitFor();
						break;
					}
				}
				System.out.println("thread_END");
			} catch (Exception e) {
	
			} finally {
				if (process != null)
					process.destroy();
			}
		}
		System.out.println("PROCESS_END");
		
		// ssh.exe task kill
		KillProcess.killSSH();
		Repository localRepo = null;
		String user = GenInit.prop.get(System.getProperty("os.name").split(" ")[0]).get("user");
		String password = GenInit.prop.get(System.getProperty("os.name").split(" ")[0]).get("password");
		File path = new File(GenInit.prop.get(System.getProperty("os.name").split(" ")[0]).get("rootDir") 
											 + System.getProperty("file.separator") + DirName);
		
		System.out.println(GenInit.prop.get(System.getProperty("os.name").split(" ")[0]).get("rootDir") 
				 + System.getProperty("file.separator") + DirName);
		
		try {
			localRepo = new FileRepository(GenInit.prop.get(System.getProperty("os.name").split(" ")[0]).get("rootDir") + "/.git");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Git git = new Git(localRepo);
		CloneCommand clone = git.cloneRepository();
		clone.setBare(false).setCloneAllBranches(true).setDirectory(path)
				.setURI(remotePath);
		UsernamePasswordCredentialsProvider user_ = new UsernamePasswordCredentialsProvider(
				user, password);
		clone.setCredentialsProvider(user_);
		try {
			clone.call();
		} catch (InvalidRemoteException e) {
			e.printStackTrace();
		} catch (TransportException e) {
			e.printStackTrace();
		} catch (GitAPIException e) {
			e.printStackTrace();
		}

	}
}
