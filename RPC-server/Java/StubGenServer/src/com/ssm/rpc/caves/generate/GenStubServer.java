package com.ssm.rpc.caves.generate;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GenStubServer {
	
	private ExecutorService exeService;
	private Object key;
	private String girDir;
	private String glistDir;
	private String clonedir;
	
	public GenStubServer(int poolSize, int portNum, String girDir,
						 String glistDir, String clonedir){
		this.key = new Object();
		this.exeService = Executors.newFixedThreadPool(poolSize);
		this.girDir = girDir;
		this.glistDir = glistDir;
		this.clonedir = clonedir;
		runServ(portNum);
	}
	
	public void runServ(int portNum){
		try {
			// portNum은 추후 변환
			ServerSocket server = new ServerSocket(portNum);
			System.out.println("TCP/IP : Wait for Web's request.");
			while (true) {
				System.out.println("while");
				synchronized (key) {
					Socket sock = server.accept();
					StubGenWorker worker = new StubGenWorker(sock, girDir, glistDir, clonedir); 
 					exeService.execute(worker);
				}
			} // while
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
}
