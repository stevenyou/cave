/*
 * "git connect" 내장 명령
 *
 * Copyright (C) 2014 JK Kim
 *
 * cave-rpc-server를 설치할 때 .profile에
 * export CAVE_RPC_INFO_PATH='.cave-rpc-info 디렉토리 경로'
 * 를 추가해야함.
 */

#include "builtin.h"
#include "exec_cmd.h"
#include "parse-options.h"

static const char * const builtin_connect_usage[] = {
	N_("git connect [ls | list]"),
	N_("git connect add <project_name> [<project_path]"),
	N_("git connect rename <old_project_name> <new_project_name>"),
	N_("git connect rm <project_name>"),
	NULL,
};

int ls(char *project_list_path){
	FILE *fp = fopen(project_list_path, "r");
	char name_buffer[20];
	char path_buffer[1000];

	if (!fp) {
		die("cannot open project-list\n"
			"Please, check if project-list exists");
	}

	printf("Name                Path\n");
	while (fscanf(fp, "%s%s", name_buffer, path_buffer) == 2) {
		printf("%-20s%s\n", name_buffer, path_buffer);
	}

	fclose(fp);
	return 0;
}

int searchProject(FILE *fp, const char *project_name) {
	char name_buffer[20];
	unsigned int length = strlen(project_name);

	while (fscanf(fp, "%s", name_buffer) == 1) {
		/* 길이 비교 */
		if (length == strlen(name_buffer)) {
			/* 글자 비교 */
			if (!strcmp(project_name, name_buffer)) {
				return 1;
			}
		}
		while ('\n' != fgetc(fp));
	}

	return 0;
}

int add(char *project_list_path, int argc, const char **argv){
	FILE *fp = fopen(project_list_path, "a+");
	const char *path = argv[1];

	if (!fp) {
		die("cannot open project-list\n"
			"Please, check if project-list exists");
	}

	if (argc > 0 && argc < 3) {
		/* 프로젝트 리스트에 같은 이름이 있는지 검색 */
		if (searchProject(fp, argv[0])) {
			die("This name already exists");
		}

		/* 경로를 입력하지 않은 경우 등록할 경로 추출 */
		if (argc == 1)
			path = get_git_work_tree();
		else
			path = argv[1];

		/* 프로젝트 리스트에 등록 */
		fprintf(fp, "%-20s %s\n", argv[0], path);
	} else {
		die("Argument error\n"
			 "Usage: git connect add <project_name> [<project_path]");
	}

	fclose(fp);
	return 0;
}

int mv(char *project_list_path, int argc, const char **argv){
	FILE *fp = fopen(project_list_path, "r");
	FILE *temp;
	char *temp_path;
	char buffer[2048];
	unsigned int length;
	struct strbuf temp_strbuf = STRBUF_INIT;

	strbuf_addstr(&temp_strbuf, project_list_path);
	strbuf_addstr(&temp_strbuf, ".tmp");
	temp_path = temp_strbuf.buf;

	if (!fp) {
		die("cannot open project-list\n"
			"Please, check if project-list exists");
	}

	if (argc == 2) {
		/* 임시 파일 open */
		temp = fopen(temp_path, "w");
		length = strlen(argv[0]);

		/* 임시 파일로 옮기면서 해당 라인 삭제 */
		while (fgets(buffer, 2048, fp) != NULL) {
			if (buffer[length] == ' ' && !strncmp(buffer, argv[0], length)) {
				strncpy(buffer, argv[1], length);
				for (; length <20; length++)
					buffer[length] = ' ';
			}
			fputs(buffer, temp);
		}

		/* 원본 삭제 후 임시 파일 이름 변경*/
		if (!remove(project_list_path))
			rename(temp_path, project_list_path);
	} else {
		die("Argument error\n"
			 "Usage: git connect rename <old_project_name> <new_project_name>");
	}

	strbuf_release(&temp_strbuf);
	fclose(fp);
	return 0;
}

int rm(char *project_list_path, int argc, const char **argv){
	FILE *fp = fopen(project_list_path, "r");
	FILE *temp;
	char *temp_path;
	char buffer[2048];
	unsigned int length;
	struct strbuf temp_strbuf = STRBUF_INIT;

	strbuf_addstr(&temp_strbuf, project_list_path);
	strbuf_addstr(&temp_strbuf, ".tmp");
	temp_path = temp_strbuf.buf;

	if (!fp) {
		die("cannot open project-list\n"
			"Please, check if project-list exists");
	}

	if (argc == 1) {
		/* 임시 파일 open */
		temp = fopen(temp_path, "w");
		length = strlen(argv[0]);

		/* 임시 파일로 옮기면서 해당 라인 삭제 */
		while (fgets(buffer, 2048, fp) != NULL) {
			if (buffer[length] == ' ' && !strncmp(buffer, argv[0], length)) {
				continue;
			}
			fputs(buffer, temp);
		}

		/* 원본 삭제 후 임시 파일 이름 변경*/
		if (!remove(project_list_path))
			rename(temp_path, project_list_path);
	} else {
		die("Argument error\n"
			 "Usage: git connect rm <project_name>");
	}

	strbuf_release(&temp_strbuf);
	fclose(fp);
	return 0;
}

int cmd_connect(int argc, const char **argv, const char *prefix){
	char *cave_rpc_info_path;
	struct strbuf project_list_path = STRBUF_INIT;
	struct option options[] = {
		OPT_END(),
	};
	int result;

	/*if (argc == 2 && !strcmp(argv[1], "-h"))
		usage_with_options(builtin_connect_usage, options);
	*/
	argc = parse_options(argc, argv, prefix, options, builtin_connect_usage,
		PARSE_OPT_STOP_AT_NON_OPTION);

	/* ~/.cave_rpc_info 디렉토리의 존재를 확인 */
	cave_rpc_info_path = getenv("CAVE_RPC_INFO_PATH");
	if (!cave_rpc_info_path)
		die("cave-rpc-info does not exist\n"
			"Please, install cave-rpc-server");

	/* project-list 경로 */
	strbuf_addstr(&project_list_path, cave_rpc_info_path);
	strbuf_addstr(&project_list_path, "/project-list");
	
		/* 명령 처리 */
	if (argc > 0) {
		if (!strcmp(argv[0], "ls") || !strcmp(argv[0], "list"))
			result = ls(project_list_path.buf);
		else if (!strcmp(argv[0], "add"))
			result = add(project_list_path.buf, argc - 1, argv + 1);
		else if (!strcmp(argv[0], "rename"))
			result = mv(project_list_path.buf, argc - 1, argv + 1);
		else if (!strcmp(argv[0], "rm"))
			result = rm(project_list_path.buf, argc - 1, argv + 1);
		else {
			error(_("Unknown subcommand: %s"), argv[0]);
			usage_with_options(builtin_connect_usage, options);
		}
	} else {
		usage_with_options(builtin_connect_usage, options);
	}

	strbuf_release(&project_list_path);
	return result ? 1 : 0;
}
